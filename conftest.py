import os
import sys
from pathlib import Path

import django


def pytest_configure():
    sys.path.insert(0, str(Path(__file__).parent))
    sys.path.insert(0, str(Path(__file__).parent / 'src'))

    os.environ['DJANGO_SETTINGS_MODULE'] = 'tests.settings'
    django.setup()
