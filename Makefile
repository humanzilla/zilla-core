.PHONY: default tests lint clean fullclean

CURRENT_DIR=$(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))

install: venv
	./venv/bin/pip install -e $(CURRENT_DIR)[testing]

venv:
	python3.7 -m venv venv
	./venv/bin/pip install -U pip setuptools

tests: clean
	./venv/bin/pytest -vv -x \
		--doctest-modules \
		--cov=src/zilla \
		--cov-report=term \
		--cov-report=html \
		--cov-append \
		\
		tests/ \
		src/zilla

lint:
	flake8 src/zilla
	isort --check-only --diff --recursive src/zilla

todo:
	@-grep --exclude-dir=node_modules --exclude-dir=vendors -rnH TODO src

clean:
	find src -iname "*.pyc" -delete
	find tests -iname "*.pyc" -delete

fullclean: clean
	python setup.py clean --all

	rm -rf *.egg-info/
	rm -rf dist/
