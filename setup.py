from setuptools import setup, find_packages

testing_requires = """
pytest
pytest-cov
pytest-django
pytest-pdb
""".strip().split('\n')

install_requires = """
django>2.1
psycopg2-binary

lxml
cssselect
premailer
pydash
bleach
yattag
bson
requests
Pillow
markdown2
colorthief
pilkit
PyYAML
yattag
graphviz
tailhead

djangorestframework>3.8

django-filter
django-jsoneditor
""".strip().split('\n')

setup(
    name='zilla',
    version='1.0.0',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    zip_safe=False,
    include_package_data=True,
    install_requires=install_requires,
    extras_require={
        'testing': testing_requires,
    },
)
