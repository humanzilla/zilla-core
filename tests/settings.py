DEBUG = False

SECRET_KEY = 'secret'

# Faster user creation
PASSWORD_HASHERS = ['django.contrib.auth.hashers.MD5PasswordHasher']
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# Makes easy to match to known error messages
LANGUAGE_CODE = 'en'

STATIC_URL = '/static/'

INSTALLED_APPS = [
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.admin.apps.SimpleAdminConfig',
    'django.contrib.staticfiles',
    'tests.db_fields'
]

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'local': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
}