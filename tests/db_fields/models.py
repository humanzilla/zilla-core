from django import forms
from zilla.db import models


class DemoModel(models.Model):
    props = models.PropertiesField()
    props_many = models.PropertiesField(many=True)


class DemoForm(forms.Form):
    name = forms.CharField()
    number = forms.IntegerField()


class DemoValidatedModel(models.Model):
    props = models.PropertiesField(form_class=DemoForm)
    props_many = models.PropertiesField(many=True, form_class=DemoForm)
