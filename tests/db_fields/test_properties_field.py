import pytest
from django.core.exceptions import ValidationError
from .models import DemoModel, DemoValidatedModel


def test_default_value():
    instance = DemoModel()
    instance.full_clean()
    
    assert instance.props == {}


def test_default_value_for_many():
    instance = DemoModel()
    instance.full_clean()

    assert instance.props_many == []


def test_form_validate():
    instance = DemoValidatedModel()
    instance.props_many = 100
    
    with pytest.raises(ValidationError) as error:
        instance.full_clean()

    assert 'props_many' in error.value.message_dict