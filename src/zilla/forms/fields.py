from django.contrib.postgres.forms import SimpleArrayField

from zilla.forms.widgets import FlagsCheckboxSelectMultiple


class FlagsField(SimpleArrayField):
    widget = FlagsCheckboxSelectMultiple

    def to_python(self, value):
        if isinstance(value, (list, tuple)):
            value = ', '.join(value)
        return super().to_python(value)

    def prepare_value(self, value):
        value = super().prepare_value(value)
        if value:
            return value.split(',')
        return value
