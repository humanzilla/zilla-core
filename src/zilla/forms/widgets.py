from itertools import chain

from django.forms.widgets import Widget, CheckboxSelectMultiple
from django.utils.html import format_html
from django.utils.safestring import mark_safe


class WidgetWithScript(Widget):
    def render_html(self, name, value, attrs):
        """Render the HTML (non-JS) portion of the field markup"""
        return super(WidgetWithScript, self).render(name, value, attrs)

    def render(self, name, value, attrs=None, renderer=None):
        # no point trying to come up with sensible semantics for when 'id' is missing from attrs,
        # so let's make sure it fails early in the process
        try:
            id_ = attrs['id']
        except (KeyError, TypeError):
            raise TypeError("WidgetWithScript cannot be rendered without an 'id' attribute")

        widget_html = self.render_html(name, value, attrs)

        js = self.render_js_init(id_, name, value)
        out = '{0}<script>{1}</script>'.format(widget_html, js)
        return mark_safe(out)

    def render_js_init(self, id_, name, value):
        return ''


class FlagsCheckboxSelectMultiple(CheckboxSelectMultiple):
    def __init__(self, *args, **kwargs):
        self.queryset = kwargs.pop('queryset')
        super(FlagsCheckboxSelectMultiple, self).__init__(*args, **kwargs)

    def value_from_datadict(self, data, files, name):
        value = super(FlagsCheckboxSelectMultiple, self).value_from_datadict(data, files, name)
        add_field_name = '%s_add' % name
        add_flags = data.get(add_field_name, None)

        if add_flags:
            value = value + add_flags.split(',')

        value = ', '.join(set(value))
        return value

    def append_choices(self, field_name, choices):
        flags = self.queryset.values_list(field_name, flat=True)
        flags = filter(lambda f: f is not None, flags)  # Remove empty lists
        flags = chain(*flags)  # Concatenate lists
        flags = set(flags)  # Remove duplicates

        values = [value for value, label in choices]
        flags = filter(lambda flag: flag not in values, flags)

        return list(chain(choices, [(value, value.title()) for value in flags]))

    def render(self, name, value, attrs=None, choices=()):
        self.choices = self.append_choices(name, self.choices)
        output = super(FlagsCheckboxSelectMultiple, self).render(name, value, attrs, choices)
        output += format_html('<ul><li><label for="id_add_flag">Add a new flag(s)</label><br>'
                              '<input id="id_{0}_add" name="{0}_add" type="text" '
                              'class="vTextField" maxlength="140" '
                              'placeholder="A single flag, or many separated by commas \",\"">'
                              '</li></ul>'.format(name))

        return mark_safe(output)
