from django.conf import settings
from django.http import HttpRequest
from django.template import context_processors


def common(request: HttpRequest):
    context = context_processors.media(request)
    context.update(context_processors.static(request))
    context.update(context_processors.i18n(request))
    context.update(context_processors.tz(request))
    context.update({'debug': settings.DEBUG})
    return context
