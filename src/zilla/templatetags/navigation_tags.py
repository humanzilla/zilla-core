import pydash as _
from django import template

from ..htmldoc import HtmlRenderer

register = template.Library()


@register.simple_tag(name='include_navigation', takes_context=True)
def do_include_navigation(context, urlpatterns: str):
    from zilla.api.viewsets import SchemaGenerator

    request = context['request']
    endpoints = SchemaGenerator(patterns=urlpatterns).get_browsable_links(request)

    html = HtmlRenderer(request)

    def in_menu(endpoints, depth=0):
        for key, endpoint in endpoints.items():
            if key.startswith('/'):
                in_menu(endpoint, depth + 1)
            else:
                if _.get(endpoint, 'metadata.in_menu', False):
                    label = _.get(endpoint, 'metadata.label', endpoint['path'])

                    with html.div(classname=f'navigation-item'):
                        with html.tag('a', href=endpoint['path'], classname='navigation-link'):
                            html.text(label)

    with html.tag('nav', classname='navigation'):
        in_menu(endpoints)

    return html
