from django import template
from django.template.loader import get_template

from zilla.api import serializers

register = template.Library()

default_styles = {
    serializers.Field: {
        'input_type': 'text',
        'template': 'serializers/input.html'
    },
    serializers.EmailField: {
        'input_type': 'email'
    },
    serializers.PasswordField: {
        'input_type': 'password'
    },
    serializers.URLField: {
        'input_type': 'url'
    },
    serializers.IntegerField: {
        'input_type': 'number'
    },
    serializers.FloatField: {
        'input_type': 'number'
    },
    serializers.DateTimeField: {
        'input_type': 'datetime-local'
    },
    serializers.DateField: {
        'input_type': 'date'
    },
    serializers.TimeField: {
        'input_type': 'time'
    },
    serializers.FileField: {
        'input_type': 'file'
    },
    serializers.BooleanField: {
        'input_type': 'checkbox',
        'template': 'serializers/checkbox.html'
    },
    serializers.ChoiceField: {
        'input_type': 'select'
    },
    serializers.MultipleChoiceField: {
        'input_type': 'checkbox',
        'template': 'serializers/multiple_choice.html'
    },
    serializers.RelatedField: {
        'input_type': 'select',
        'template': 'serializers/related_field.html'
    },
    serializers.ManyRelatedField: {
        'input_type': 'select',
        'template': 'serializers/many_related.html'
    },
    serializers.Serializer: {
        'input_type': 'fieldset',
        'template': 'serializers/fieldset.html'
    },
    serializers.ListSerializer: {
        'input_type': 'fieldset',
        'template': 'serializers/fieldset_list.html'
    },
}

default_style = default_styles[serializers.Field]


@register.simple_tag(takes_context=True)
def serializer_field(context, field):
    style = dict(
        **default_styles[serializers.Field],
        **default_styles.get(field._field.__class__, {})
    )

    field = field.as_form_field()
    template = get_template(style['template'])

    return template.render({
        'field': field,
        'name': field.name,
        'style': style,
        'bound': context['request'].method == 'POST'
    })
