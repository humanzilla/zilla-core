import re
import uuid
from itertools import zip_longest
from urllib import parse

from django import template
from django.forms import model_to_dict
from django.template import Node, TemplateSyntaxError, VariableDoesNotExist
from django.utils.module_loading import import_string
from django.utils.safestring import mark_safe
from markdown2 import Markdown

from zilla.utils import json
from zilla.utils.analytics import get_client_fingerprint
from zilla.utils.text import readablesize

register = template.Library()


@register.simple_tag(name='urlencode')
def do_urlencode(**kwargs):
    """
    >>> do_urlencode(a=1, b=2)
    'a=1&b=2'
    >>> do_urlencode(a=1, b=2, c=None)
    'a=1&b=2'
    """
    kwargs = {key: value for (key, value) in kwargs.items() if value}
    return parse.urlencode(kwargs)


@register.simple_tag(takes_context=True)
def fingerprint(context):
    return get_client_fingerprint(context['request'])


@register.simple_tag(name='uuid')
def generate_uuid4():
    return uuid.uuid4().hex


@register.filter()
def wraplist(items, group_size=3):
    """

    >>> list(wraplist([1,2,3,4,5], 3))
    [[1, 2, 3], [4, 5]]
    """
    args = [iter(items)] * group_size
    return (list(filter(lambda a: a is not None, group)) for group in zip_longest(*args, fillvalue=None))


@register.filter()
def evensplit(items, group_size=1):
    """

    >>> list(evensplit([1, 2, 3], 3))
    [(1, 2, 3)]
    >>> list(evensplit([1, 2, 3, 4], 3))
    [(1, 2, 3), (4, None, None)]
    >>> list(evensplit([1, 2, 3, 4, 5], 3))
    [(1, 2, 3), (4, 5, None)]

    """
    args = [iter(items)] * group_size
    return zip_longest(*args, fillvalue=None)


@register.filter(name='readablesize')
def do_readablesize(value):
    """ 1024 to 1Mb"""
    return readablesize(value)


@register.filter(name='omit')
def do_omit(items: dict, comp):
    """

    >>> do_omit({"a": 1, "b": 2}, "a")
    {'b': 2}
    >>> do_omit({"a": 1, "b": 2, "c": 3}, "a")
    {'b': 2, 'c': 3}
    """
    return {key: val for key, val in items.items() if key != comp}


@register.filter(name='indent')
def do_indent(val, num_spaces=4):
    """

    >>> do_indent("Hello\\nWorld")
    '    Hello\\n    World'
    """
    prefix = ' ' * num_spaces
    return prefix + val.replace('\n', '\n' + prefix)


@register.filter(name='startswith')
def do_startswith(val, prefix):
    """

    >>> do_startswith('my name', 'my')
    True

    >>> do_startswith('my name', 'name')
    False
    """

    return str(val).startswith(prefix)


@register.filter(name='json')
def do_json(val):
    return mark_safe(json.dumps(val))


@register.filter(name='markdown')
def do_markdown(val):
    parser = Markdown(extras=['cuddled-lists', 'tables', 'fenced-code-blocks'])
    return mark_safe(parser.convert(val))


@register.filter(name='model_to_dict')
def do_model_to_dict(val, arg=''):
    if arg:
        fields = re.split(r'\s+', arg)
    else:
        fields = None
    return model_to_dict(val, fields)


class PushContextNode(Node):
    def __init__(self, val_expr, asvar):
        self.val_expr = val_expr
        self.asvar = asvar

    def render(self, context):
        try:
            value = self.val_expr.resolve(context)
        except VariableDoesNotExist:
            return ''

        context[self.asvar] = value

        return ''


@register.tag(name='let')
def do_setvalue(parser, token):
    """
    {% let variable.var.var.0 as newvar %}
    {{ newvar }}
    """

    bits = token.split_contents()

    if len(bits) == 4:
        tag, this_value_expr, as_, asvar = bits
        if as_ != 'as':
            raise TemplateSyntaxError("Invalid syntax in push tag. Expecting 'as' keyword")

    else:
        raise TemplateSyntaxError("push takes at least one argument arguments")

    return PushContextNode(val_expr=parser.compile_filter(this_value_expr), asvar=asvar)


@register.filter('hasattr')
def do_hasattr(value, name: str):
    """
    Check if the object owns the attribute by name


    >>> import os
    >>> class Foo:
    ...     attr2 = None

    >>> do_hasattr(os, 'path')
    True
    >>> do_hasattr(Foo(), 'attr2')
    True
    >>> do_hasattr(Foo(), 'attrX')
    False
    >>> do_hasattr(Foo, 'attr')
    False

    """
    return hasattr(value, name)


@register.simple_tag(name='import')
def do_import_string(path):
    return import_string(path)


@register.simple_tag(name='urlstartswith', takes_context=True)
def do_urlstartswith(context: dict, path: str):
    if context['request'].path_info.startswith(path):
        return 'active'


@register.simple_tag(name='urlequal', takes_context=True)
def do_urlequal(context: dict, path: str):
    if context['request'].path_info.strip('/') == path.strip('/'):
        return 'active'


@register.filter(name='negate')
def do_negate(value):
    return not value
