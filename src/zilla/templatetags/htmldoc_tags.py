from django import template
from django.conf import settings
from django.http import HttpRequest

from zilla.contrib.hooks.registry import hooks
from ..htmldoc import HtmlRenderer
from ..utils import json

register = template.Library()


@register.simple_tag(takes_context=True)
def render_head(context, **kwargs):
    context.update(kwargs)

    request = context['request']  # type: HttpRequest
    request.dev_mode = settings.DEBUG and "browser-sync" == request.META.get('HTTP_X_FORWARDED_BY', None)

    html = HtmlRenderer(request)

    if 'title' in context:
        with html.tag('title'):
            html.text(context['title'])

    html.meta(name="charset", content="utf-8")
    html.meta(name='viewport', content="width=device-width, initial-scale=1")
    html.meta(**{'http-equiv': 'x-dns-prefetch-control', 'content': 'on'})
    html.meta(**{'http-equiv': 'X-UA-Compatible'}, content="IE=edge")

    initial_data = {
        'version': settings.DEPLOY_VERSION,
        'modes': {'dev': request.dev_mode, 'debug': settings.DEBUG, 'test': False},
        'trustedHost': request.get_host(),
        'urls': {
            'media': settings.MEDIA_URL,
            'static': settings.STATIC_URL,
        }
    }

    if hasattr(request, 'parser_context'):
        view = request.parser_context['view']
        initial_data['view'] = {'name': view.get_view_name(),
                           'action': view.get_action_name(), }

    hooks.dispatch('register_initial_data', request, initial_data)

    with html.tag('script', nonce=request._nonce):
        context = "window.__initialData__={0};\n".format(json.dumps(initial_data))
        html.doc.asis(context)

    hooks.dispatch('after_html_head', request, html)

    html.preload_scripts()
    html.render_stylesheets()

    hooks.dispatch('before_html_head', request, html)

    return html


@register.simple_tag(takes_context=True)
def render_endbody(context):
    request = context['request']

    html = HtmlRenderer(request)

    with html.tag('script', id="shared-data", type="application/json"):
        html.text(json.dumps(request.shared_data))

    with html.tag('script', nonce=request._nonce):
        html.text('window.__sharedData__ = '
                  'JSON.parse(document.getElementById("shared-data").textContent '
                  '|| "{}") || {}')

    html.render_scripts()

    hooks.dispatch('before_body_end', request, html)

    return html
