import os
import re
from collections import defaultdict
from itertools import chain

from django.core.management.base import BaseCommand
from django.template.loaders.app_directories import get_app_template_dirs

extract_extends = re.compile(r'{% extends ["\']([^\"^\'].+)[\"\'].*%}', re.MULTILINE | re.IGNORECASE).match


class Command(BaseCommand):
    def handle(self, *args, **options):

        template_files = defaultdict(list)

        for template_dir in get_app_template_dirs('templates'):
            for dir, dirnames, filenames in os.walk(template_dir):
                for filename in filenames:
                    if filename.endswith('.html'):
                        dir1, dir2 = dir.split('/templates', 1)
                        dir2 = dir2.strip('/')
                        package = dir1.split('/')[-1]

                        if package in ['admin', 'admindocs', 'django_ses',
                                       'rest_framework_swagger', 'reversion',
                                       'jet', 'rest_framework', 'referrers']:
                            continue

                        with open(os.path.join(dir, filename), 'rt') as tpl:
                            match = extract_extends(tpl.read())
                            template_name = os.path.join(dir2, filename)

                            if match:
                                name = [template_name, match.group(1)]
                            else:
                                name = [template_name]

                        template_files[package].append(name)

        print("@startuml")
        print("left to right direction")

        rels = filter(lambda a: len(a) == 2, chain(*template_files.values()))

        for group, lines in template_files.items():
            print(f'package "{group}" {{')

            for name in lines:
                if len(name) == 2:
                    print(f'  [{name[0].strip()}]')

            print(f"}}")

        for rel in rels:
            print(f'  [{rel[0]}] --* [{rel[1]}]')

        print("@enduml")
