from importlib import import_module

from django.conf import settings
from django.contrib.sessions.models import Session
from django.core.management.base import BaseCommand

from ..utils.sessions import delete_all_user_sessions

session_engine = import_module(settings.SESSION_ENGINE)


class Command(BaseCommand):
    help = "Log out all users."

    def handle(self, *args, **options):
        count = Session.objects.all().count()
        self.stdout.write(f'{count} stored sessions.')
        delete_all_user_sessions()
