import getpass

from django.core.management.base import BaseCommand


class SerializerCommand(BaseCommand):
    serializer_class = None

    def handle(self, *args, **options):
        assert self.serializer_class is not None, "You must define a serializer_class for the command"

        serializer = self.serializer_class(data=options)

        first_check = True

        while not serializer.is_valid():

            if not first_check:
                for key, value in serializer.items():
                    errors = '\n\t'.join(value)
                    print(f'[Error] {key}: {errors}')

            options = options.copy()

            for name, errors in serializer.errors.items():
                field = serializer[name]

                if field.help_text:
                    print(f'({field.help_text})')

                if any(map(lambda word: word in field.name, ['password', 'secret'])):
                    options[name] = getpass.getpass(f'{field.label} (secret): ')
                else:
                    options[name] = input(f'{field.label}: ')

            serializer = self.serializer_class(data=options)

        self.serializer_valid(serializer, **options)

    def serializer_valid(self, serializer, **options):
        pass
