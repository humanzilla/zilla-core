from django.apps import AppConfig


class HooksConfig(AppConfig):
    name = 'zilla.contrib.hooks'

    def ready(self):
        from zilla.contrib.hooks.registry import hooks
        hooks.search_for_items()
