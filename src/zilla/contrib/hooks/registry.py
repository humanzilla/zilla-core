from zilla.registry.base import BaseRegistry, BaseItem


class Action(BaseItem):
    pass


class HooksRegistry(BaseRegistry):
    item_class = Action


hooks = HooksRegistry()
