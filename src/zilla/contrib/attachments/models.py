import hashlib
import os
import textwrap
from io import BytesIO
from mimetypes import MimeTypes
from typing import Tuple

import requests
from django.conf import settings
from django.core.files import File
from django.core.files.images import ImageFile
from django.core.files.storage import default_storage, get_storage_class
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.utils.timezone import now
from PIL import Image
from pilkit.utils import save_image

from zilla.db import models
from zilla.utils.image import get_exif, image_fit_width, normalize_orientation, square_crop
from zilla.utils.text import readablesize

mime = MimeTypes()


def get_checksum(fobj: File, salt=None):
    hasher = hashlib.sha1()
    if salt:
        hasher.update(salt.encode())
    for chunk in fobj.chunks():
        hasher.update(chunk)
    return hasher.hexdigest()


def upload_path(instance, filename, **kwargs):
    checksum = get_checksum(instance.attach, settings.SECRET_KEY)
    base, ext = os.path.splitext(filename)
    prefix = '/'.join(textwrap.wrap(checksum[:24], 2))
    return f'uploads/{prefix}/{checksum}{ext}'


class AttachmentManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().defer('checksum', 'size', 'mimetype', 'private')

    def create_for_url(self, url):
        file_name = url.split("/")[-1].split('#')[0].split('?')[0]
        buf = BytesIO()
        response = requests.get(url)
        buf.write(response.content)
        obj = InMemoryUploadedFile(buf, 'attach', file_name, None, buf.tell(), None)
        record = (self.model()
                  .set_attach(obj)
                  .save())
        return record


class AttachmentStorage(get_storage_class()):
    """Reuse files depending on the sha1sum. Avoid duplicates"""

    # TODO: Convert into a patch to the default upload storage instead. Ej. be able use S3

    def get_available_name(self, name, max_length=None):
        return name

    def _save(self, name, content):
        if self.exists(name):
            return name
        return super()._save(name, content)


class BaseAttachment(models.MutableModel):
    # Relate an attachment by sending the permalink or uploading a file stream.
    # aka: How telegram upload files
    attach = models.FileField(upload_to=upload_path, max_length=255, storage=AttachmentStorage())
    permalink = models.PermalinkField(primary_key=True)
    size = models.PositiveIntegerField(null=True, editable=False)
    mimetype = models.CharField(max_length=100, blank=True, editable=False)
    checksum = models.CharField(max_length=64, blank=True, db_index=True, editable=False)
    private = models.BooleanField(default=False, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now_add=True)
    props = models.PropertiesField()

    objects = AttachmentManager()

    class Meta:
        abstract = True
        verbose_name = 'adjunto'
        verbose_name_plural = 'adjuntos'

    def __str__(self):
        cls = type(self).__name__
        return '<%s %s at 0x%x>' % (cls, self.pk, id(self))

    def get_absolute_url(self):
        if self.private:
            # If private generate a unique url, that can be
            # discarded by restart the secret string
            return f'/attachment/{self.get_secret()}/'
        return self.attach.url

    def get_secret(self):
        """Return the secret screen """
        salt = '%s%s' % (str(self.checksum), str(self.modified))
        secret = hashlib.sha1(salt.encode).hexdigest()
        return f'{self.pk.hex}.{secret.int}'

    def get_size(self):
        return readablesize(self.size)

    get_size.short_description = 'Size'
    get_size.admin_order_field = 'size'

    def restart_secret(self):
        """Force to create a secret string"""
        self.modified = now()
        self.save(update_fields=['modified'])

    def set_attach(self, upload):
        # TODO: Set as the prefered method to add the file.
        # better, enforce it to just use this method.
        # TODO: Check if there is a file with the same checksum, if there is reuse it.
        self.attach = File(upload, upload.name)
        return self

    def save(self, **kwargs):
        self.mimetype = mime.guess_type(self.attach.url)[0]
        self.checksum = get_checksum(self.attach)
        self.size = self.attach.size
        return super().save(**kwargs)

    @staticmethod
    def cleanup_attachment_files(sender, instance, **kwargs):
        instance.attach.delete(False)
        for key, value in instance.props.get('thumbnails', {}).items():
            try:
                default_storage.delete(instance.props['thumbnails'][key]['url'])
            except KeyError:
                pass

    @classmethod
    def register_signals(cls):
        receiver(pre_delete, sender=cls)(cls.cleanup_attachment_files)


THUMBNAIL_ALIAS = [
    ('32x32', (32, 32)),
    ('90x90', (90, 90)),  # ios
    ('180x180', (180, 180)),  # ios retina
    ('512x512', (512, 512)),  # fb news feed

    ('256x', (256, None)),
    ('512x', (512, None)),  # fb news feed
    ('720x', (720, None)),
    ('1024x', (1024, None))
]


class PhotoManager(AttachmentManager):
    def get_queryset(self):
        return super().get_queryset().defer('exif', 'cropping')


class PhotoMixin(models.Model):
    height = models.PositiveIntegerField(default=0, editable=False)
    width = models.PositiveIntegerField(default=0, editable=False)

    caption = models.CharField(max_length=255, blank=True)
    exif = models.PropertiesField()
    objects = PhotoManager()

    class Meta:
        abstract = True
        verbose_name = 'foto'
        verbose_name_plural = 'fotos'

    def set_attach(self, upload):
        self.attach = ImageFile(upload, upload.name)
        return self

    def get_image(self):
        return Image.open(BytesIO(self.attach.read()))

    def generate_thumbnails(self, aliases: Tuple = None, recreate=False):
        if not self.attach:
            pass

        image = self.get_image()
        image = normalize_orientation(image)
        filename, fileext = os.path.splitext(self.attach.name)

        if recreate:
            thumbnails = {}
        else:
            thumbnails = self.props.get('thumbnails', {})

        sizes = THUMBNAIL_ALIAS

        if aliases:
            sizes = filter(lambda alias: alias[0] in aliases, sizes)

        for alias, size in sizes:
            if alias in thumbnails:
                # Skip if thumbnail already generated
                continue

            width, height = size

            if height:
                # TODO: Implement smarter resize and crop
                thumbnail = square_crop(image, width, height)
            else:
                thumbnail = image_fit_width(image, width)

            file = File(BytesIO(), name=f'{filename}.{alias}.jpg')

            save_image(img=thumbnail,
                       outfile=file,
                       format='JPEG',
                       options={'quality': 99, 'optimize': True, 'progressive': True})

            file.seek(0)

            thumb_path = default_storage.save(name=file.name, content=file)
            thumbnails[alias] = {'url': thumb_path,
                                 'height': thumbnail.height,
                                 'width': thumbnail.width}

        if not self.props:
            self.props = {}

        # TODO: Memory problems when trying to store the exif data
        # TODO: psycopg2.OperationalError: index row requires 54280 bytes, maximum size is
        self.exif = get_exif(image)
        self.props.update({
            'thumbnails': thumbnails,
        })

        self.save(update_fields=['props', 'exif'])

    def save(self, **kwargs):
        image = ImageFile(self.attach.file)
        self.height = image.height
        self.width = image.width
        super().save(**kwargs)
