from django import forms
from django.conf import settings
from django.core.files.uploadedfile import UploadedFile
from django.db import models
from django.forms import widgets


class PhotoImageField(widgets.Input):
    template_name = 'widgets/photo.html'
    needs_multipart_form = True

    def format_value(self, value):
        if value:
            return value.attach

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['MEDIA_URL'] = settings.MEDIA_URL
        context['instance'] = value
        return context

    def value_from_datadict(self, data, files, name):
        if f'{name}__attach' in files:
            return files.get(f'{name}__attach')
        return data[name]


class AttachmentUploadField(forms.ModelChoiceField):
    widget = PhotoImageField

    def prepare_value(self, value):
        value = super().prepare_value(value)
        model = self.queryset.model

        if value:
            attach = model.objects.get(pk=value)
            value = attach.specific

        return value

    def to_python(self, value):
        if value in self.empty_values:
            return None

        if isinstance(value, UploadedFile):
            # If the value is an uploaded file, just create the record.
            attach = self.queryset.create(attach=value)
            value = attach.specific
        else:
            try:
                key = self.to_field_name or 'pk'
                value = self.queryset.get(**{key: value})
            except (ValueError, TypeError, self.queryset.model.DoesNotExist):
                raise forms.ValidationError(self.error_messages['invalid_choice'], code='invalid_choice')
        return value


class AttachmentField(models.ForeignKey):
    def __init__(self, to, **kwargs):
        kwargs.setdefault('related_name', "%(app_label)s_%(class)s_related")
        kwargs.setdefault('related_query_name', "%(app_label)s_%(class)ss")
        kwargs.setdefault('null', True)

        # TODO: Check model is an Attachment model
        kwargs['to'] = to

        if kwargs.get('null', False):
            kwargs['on_delete'] = models.SET_NULL

        super().__init__(**kwargs)

    def value_to_string(self, obj):
        return self.get_prep_value(obj.url)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        args = []
        if 'null' in kwargs:
            del kwargs['null']
        if 'related_name' in kwargs:
            del kwargs['related_name']
        return name, path, args, kwargs
