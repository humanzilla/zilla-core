from django.core.files.storage import Storage


class OverwriteStorage(Storage):
    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            self.delete(name)
        return super().get_available_name(name, max_length=max_length)
