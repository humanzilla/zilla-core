from django.apps import AppConfig
from django.db.models.signals import post_migrate


def create_defaults_enum_values(sender, **kwargs):
    from zilla.contrib.enum import get_enum_models
    enum_models = get_enum_models()

    for model_class in enum_models:
        for name, label in model_class.DEFAULTS:
            model_class.objects.get_or_create(name=name, defaults={'label': label})


class EnumConfig(AppConfig):
    name = 'zilla.contrib.enum'

    def ready(self):
        post_migrate.connect(create_defaults_enum_values, sender=type(self))
