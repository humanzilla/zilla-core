from collections import defaultdict

ENUM_MODELS = defaultdict(list)


def get_enum_models():
    return ENUM_MODELS


def append_enum_model(enum_model):
    if enum_model not in ENUM_MODELS[enum_model]:
        ENUM_MODELS[enum_model].append(enum_model)
