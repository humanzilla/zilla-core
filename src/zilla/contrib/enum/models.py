from django.db import models
from django.utils.text import slugify

from zilla.contrib.enum import append_enum_model


class EnumBase(models.base.ModelBase):
    def __new__(cls, name, bases, attrs):
        super_new = super().__new__(cls, name, bases, attrs)

        if not super_new._meta.abstract:
            append_enum_model(super_new)

        return super_new


class EnumManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().defer('props')


class BaseEnumModel(models.Model, metaclass=EnumBase):
    """Enumeration Model"""
    name = models.CharField(max_length=100, primary_key=True, db_index=True)
    label = models.CharField(max_length=100)
    order = models.IntegerField(unique=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    objects = EnumManager()

    class Meta:
        abstract = True

    def __str__(self):
        return self.label or self.name

    def save(self, **kwargs):
        if not self.id:
            # Set the order to the next high value in the records
            latest = (type(self)._default_manager
                      .select_for_update()
                      .order_by('-order')
                      .values('order'))
            self.order = latest.get('order', 0) + 1

        if not self.name and self.label:
            self.name = slugify(self.label)
        super().save(**kwargs)
