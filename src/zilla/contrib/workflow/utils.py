from graphviz import Digraph

from zilla.contrib.workflow import Workflow


def render_as_graphviz(workflow: Workflow, base_state=None):
    dot = Digraph()

    for node, props in workflow.get_workflow():
        attrs = {}

        if props['starts']:
            attrs['shape'] = 'diamond'
            attrs['style'] = 'filled'
            attrs['color'] = 'lightgray'
        elif props['ends']:
            attrs['shape'] = 'diamond'
            attrs['style'] = 'filled'
            attrs['color'] = 'lightgray'

        if node.state == base_state:
            attrs['style'] = 'filled'
            attrs['color'] = 'blue'
            attrs['fontcolor'] = 'white'

        dot.node(node.state, node.label, **attrs)

        for transition in props['transitions']:
            dot.edge(node.state, transition.to_state, label=transition.label)

    return dot
