from django.db import models

from zilla.utils.decorators import permalink


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def __str__(self):
        return self.__class__.__name__

    @permalink
    def get_absolute_url(self):
        info = self._meta.app_label, self._meta.model_name

        return 'admin:%s_%s_change' % info, None, {'object_id': self.pk}

    @classmethod
    def get_instance(cls):
        instance = (cls.objects.filter()
                    .select_related()
                    .prefetch_related()
                    .first())

        if not instance:
            instance = cls.objects.create()

        return instance

    @classmethod
    def load(cls):
        return cls.get_instance()

    def delete(self, **kwargs):
        pass

    def update(self, **kwargs):
        self.__class__.objects.filter(pk=self.pk).update(**kwargs)

    def save(self, **kwargs):
        instance = self.__class__.objects.filter().first()

        if instance:
            self.pk = instance.pk

        self.full_clean()
        super().save(**kwargs)
