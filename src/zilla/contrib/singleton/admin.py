from django.contrib import admin
from django.core.exceptions import ImproperlyConfigured

from zilla.utils.requests import redirect
from .models import SingletonModel


class SingletonAdmin(admin.ModelAdmin):
    def __init__(self, model: SingletonModel, admin_site):
        if isinstance(model, SingletonModel):
            raise ImproperlyConfigured('Admin class is just for singleton based models')

        super().__init__(model, admin_site)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_object(self, request, object_id, from_field=None):
        return self.model.get_instance()

    def change_view(self, request, object_id, form_url='', extra_context=None):
        instance = self.model.get_instance()

        if str(instance.pk) == object_id:
            return super(SingletonAdmin, self).change_view(request, object_id, form_url, extra_context)

        return redirect(instance.get_absolute_url())

    def changelist_view(self, request, extra_context=None):
        instance = self.model.get_instance()
        return redirect(f'{instance.pk}/')
