from django.urls import path

from zilla.views import page_view, debug_view

content_types = {
    'json': 'application/json',
    'html': 'text/html',
    'xml': 'text/xml',
    'txt': 'text/plain',
    'md': 'text/plain',
}

seo_urls = [
    path("manifest.json", page_view, {'template_name': 'manifest.json'}, name='manifest.json'),
    path("browserconfig.xml", page_view, {'template_name': 'browserconfig.xml'}, name='browserconfig.xml'),
    path("humans.txt", page_view, {'template_name': 'humans.txt'}),
    path("robots.txt", page_view, {'template_name': 'robots.txt'}),
    path("ping.txt", page_view, {'template_name': 'ping.txt'}),
]

debug_urls = [
    path("__debug__/", debug_view)
]
