from functools import wraps

from zilla.api.response import Response


def action_handler(serializer_class=None, detail=False, **kwargs):
    """
    Factory decorator that creates a action route with the given serializer class
    """
    methods = ['get', 'post']
    kwargs['serializer_class'] = serializer_class

    def decorator(func):
        func.bind_to_methods = methods
        func.detail = detail
        func.kwargs = kwargs

        @wraps(func)
        def wrapper(instance, request):
            if request.method == 'POST':
                serializer = instance.get_serializer(data=request.data)

                if serializer.is_valid():
                    return func(instance, request, serializer)

                return Response({'errors': serializer.errors}, status=400, context={'serializer': serializer})

            serializer = instance.get_serializer()
            return Response({'serializer': serializer}, status=200, context={'serializer': serializer})

        wrapper.inner = func
        return wrapper

    return decorator


def metadata(**kwargs):
    """Adds metadata to the endpoint, will be available in the schema"""

    def decorator(func):
        func.metadata = kwargs
        return func

    return decorator
