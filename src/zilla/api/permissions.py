from rest_framework.permissions import *


class DisallowAny(BasePermission):
    def has_permission(self, request, view):
        return False
