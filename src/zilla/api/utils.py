import getpass

from .metadata import Metadata


def describe_serializer(serializer):
    return Metadata().get_serializer_info(serializer)


def prompt_serializer(serializer_class, *args, **kwargs):
    serializer = serializer_class(*args, **kwargs)

    first_check = True

    while not serializer.is_valid():

        if not first_check:
            for key, value in serializer.items():
                errors = '\n\t'.join(value)
                print(f'[Error] {key}: {errors}')

        data = {}

        for name, errors in serializer.errors.items():
            field = serializer[name]

            if field.help_text:
                print(f'({field.help_text})')

            if any(map(lambda word: word in field.name, ['password', 'secret'])):
                data[name] = getpass.getpass(f'{field.label} (secret): ')
            else:
                data[name] = input(f'{field.label}: ')

        kwargs['data'] = data
        serializer = serializer_class(*args, **kwargs)

    return serializer
