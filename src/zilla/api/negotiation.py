from rest_framework.negotiation import DefaultContentNegotiation


class ContentNegotiation(DefaultContentNegotiation):
    pass
