from rest_framework.serializers import Serializer
from rest_framework.utils import encoders

from zilla.api.metadata import Metadata


class JSONEncoder(encoders.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Serializer):
            return Metadata().get_serializer_info(obj)
        return super(JSONEncoder, self).default(obj)
