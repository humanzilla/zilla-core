from rest_framework.serializers import *


class PasswordField(CharField):
    def __init__(self, **kwargs):
        kwargs['style'] = {'input_type': 'password'}
        super().__init__(**kwargs)
