from rest_framework.response import Response as BaseResponse
from rest_framework.serializers import BaseSerializer

from zilla.api.utils import describe_serializer


class Response(BaseResponse):
    _context = {}

    def __init__(self, data: dict, context: dict = None, **kwargs):
        if context:
            self._context = context
        if isinstance(data, BaseSerializer):
            kwargs['data'] = describe_serializer(data)
        else:
            kwargs['data'] = data
        super(Response, self).__init__(**kwargs)

    @property
    def renderer_context(self):
        return self._context

    @renderer_context.setter
    def renderer_context(self, value: dict):
        context = value
        context.update(self._context)
        self._context = context
