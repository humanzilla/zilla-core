from typing import Callable

from django.contrib.admindocs.utils import parse_docstring
from django.utils.module_loading import import_string
from rest_framework import schemas, viewsets
from rest_framework.schemas.generators import (
    insert_into,
    is_custom_action,
    is_list_view)

from .response import Response


class CoreViewSet(viewsets.GenericViewSet):
    pass


class SchemaGenerator(schemas.SchemaGenerator):
    _schema = None

    def __init__(self, *, patterns, **kwargs):
        if isinstance(patterns, str):
            kwargs['patterns'] = import_string(patterns)
        super(SchemaGenerator, self).__init__(**kwargs)

    def get_keys(self, subpath, method, view):
        if hasattr(view, 'action'):
            # Viewsets have explicitly named actions.
            action = view.action
        else:
            # Views have no associated action, so we determine one from the method.
            if is_list_view(subpath, method, view):
                action = 'list'
            else:
                action = self.default_mapping[method.lower()]

        named_path_components = [component for component
                                 in subpath.strip('/').split('/')
                                 if '{' not in component]

        if is_custom_action(action):
            action = self.default_mapping[method.lower()]
            if action in self.coerce_method_names:
                action = self.coerce_method_names[action]
            named_path_components = [f'/{a}' for a in named_path_components]
            return named_path_components + [action]

        if action in self.coerce_method_names:
            action = self.coerce_method_names[action]

        named_path_components = [f'/{a}' for a in named_path_components]
        return named_path_components + [action]

    def get_browsable_links(self, request):
        paths = []
        view_endpoints = []

        if self.endpoints is None:
            inspector = self.endpoint_inspector_cls(self.patterns, self.urlconf)
            self.endpoints = inspector.get_api_endpoints()

        for path, method, callback in self.endpoints:
            view = self.create_view(callback, method, request)
            exclude_from_schema = getattr(view, 'exclude_from_schema', False)

            if exclude_from_schema:
                continue

            path = self.coerce_path(path, method, view)
            paths.append(path)
            view_endpoints.append((path, method, view))

        # Only generate the path prefix for paths that will be included
        if not paths:
            return None

        prefix = self.determine_path_prefix(paths)
        browsable = dict()

        for path, method, view in view_endpoints:
            if not self.has_view_permissions(path, method, view):
                continue

            viewfunc = getattr(view, view.action)  # type: Callable
            title, body, metadata = parse_docstring(viewfunc.__doc__)
            subpath = path[len(prefix):]
            keys = self.get_keys(subpath, method, view)
            insert_into(browsable, keys, {'path': path, 'metadata': metadata})

        return browsable


class RootViewSet(CoreViewSet):
    schema_title = 'Schema API'
    exclude_from_schema = False
    schema_generator_class = SchemaGenerator
    schema_generator: SchemaGenerator = None

    def list(self, request):
        data = self.schema_generator.get_browsable_links(request)
        return Response({'endpoints': data})
