from django.template import loader
from rest_framework import renderers, status
from rest_framework.request import Request
from rest_framework.response import Response

from zilla.api import encoders


class BrowsableRenderer(renderers.BrowsableAPIRenderer):
    def get_default_renderer(self, view):
        return JSONRenderer()


class JSONRenderer(renderers.JSONRenderer):
    encoder_class = encoders.JSONEncoder


class HTMLRenderer(renderers.BaseRenderer):
    media_type = 'text/html'
    format = 'html'
    charset = 'utf-8'
    renderer_context = {}

    @property
    def template(self):
        return self.get_template()

    def get_template(self):
        view = self.renderer_context['view']
        viewname = view.get_view_name()
        action = view.get_action_name()
        return [f'{viewname}/{action}.html',
                f'{viewname}/view.html',
                f'{viewname}.html',
                'view.html']

    def get_context(self, renderer_context: dict, accepted_media_type: str) -> dict:
        view = renderer_context['view']
        request = renderer_context['request']  # type: Request

        if request.is_ajax() or 'partial' in accepted_media_type:
            template_base = view.template_partial
        else:
            template_base = view.template_base

        context = {'view': view,
                   'user': request.user,
                   'name': view.get_view_name(),
                   'description': view.get_view_description(),
                   'base': template_base,
                   'action': view.get_action_name(), }

        renderer_context.update(context)
        return renderer_context

    def render(self, data, accepted_media_type=None, renderer_context=None) -> str:
        self.renderer_context = self.get_context(renderer_context or {}, accepted_media_type)
        self.renderer_context['data'] = data

        request: Request = self.renderer_context['request']
        response: Response = self.renderer_context['response']

        if data:
            request.initial_state.update(data)

        status_code = response.status_code

        if status_code == status.HTTP_204_NO_CONTENT:
            response.status_code = status.HTTP_200_OK
        elif status_code == status.HTTP_202_ACCEPTED:
            response.status_code = status.HTTP_200_OK
        elif status_code == status.HTTP_201_CREATED and 'Location' in response:
            response.status_code = 302
            return ''
        elif status_code == status.HTTP_410_GONE and 'Location' in response:
            response.status_code = 302
            return ''

        content = loader.render_to_string(self.template,
                                          context=self.renderer_context,
                                          request=request)
        return content
