from django.contrib import messages
from django.core.paginator import InvalidPage, Paginator
from django.db.models import QuerySet
from django.http import Http404
from django.shortcuts import render
from django.views import View
from django.views.generic import UpdateView, CreateView

from zilla.htmldoc import SimpleDoc, getvalue
from zilla.utils.text import marksafe

__all__ = ['SimpleListView', 'SimpleCreateView', 'SimpleUpdateView']


class SimpleListView(View):
    queryset: QuerySet = None
    template_name: str = None
    filter_class = None
    paginate_by: int = 25

    filterset = None
    paginator = None
    page = None
    object_list: QuerySet = None
    is_paginated: bool = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get(self, request, **kwargs):
        self.filterset, self.queryset = self.get_filtered_queryset()

        (self.paginator,
         self.page,
         self.object_list,
         self.is_paginated) = self.get_paginated_queryset(self.queryset)

        return render(request, self.get_template_name(), {
            'object_list': self.render()
        })

    def get_queryset(self):
        return self.queryset

    def get_template_name(self):
        return self.template_name

    def get_filtered_queryset(self):
        if self.filter_class:
            filter_ = self.filter_class(self.request.GET, queryset=self.get_queryset())
            return filter_, filter_.qs
        return None, self.get_queryset()

    def get_paginated_queryset(self, queryset):
        paginator = Paginator(object_list=queryset, per_page=self.paginate_by)

        if 'page' in self.request.GET:
            page_number = self.request.GET['page']
        else:
            page_number = 1

        try:
            page_number = int(page_number)
        except ValueError:
            page_number = 1

        try:
            page = paginator.page(page_number)
        except InvalidPage:
            raise Http404('Invalid page')

        return paginator, page, page.object_list, page.has_other_pages()

    @marksafe
    def render(self):
        doc = SimpleDoc()
        return self.render_table(doc)

    def render_label(self, obj, doc: SimpleDoc):
        doc.asis(str(obj))

    def render_buttons(self, obj, doc: SimpleDoc):
        pass

    def render_row(self, obj, doc: SimpleDoc):
        tag = doc.tag

        with tag('td.text-left'):
            with tag('div.object-list-item'):
                with tag('div.d-inline-block', style='width: 1rem'):
                    doc.input(type='checkbox',
                              id=f'object-pk-{obj.pk}',
                              value=obj.pk,
                              name='object[]')

                with tag('label', **{'for': f'object-pk-{obj.pk}'}):
                    self.render_label(obj, doc)

        with tag('td.text-right'):
            self.render_buttons(obj, doc)

    def render_table(self, doc: SimpleDoc):
        doc, tag, text = doc, doc.tag, doc.text

        with tag('table.object-list.table'):
            with tag('thead'):
                with tag(f'th', colspan='2'):
                    with tag('div.object-list-item.d-flex.justify-content-between'):
                        with tag('div'):
                            with tag('div.d-inline-block', style='width: 1rem;'):
                                doc.input(id='select-all', type='checkbox', name='object[]', value='all')
                            with tag('label', **{'for': 'select-all'}):
                                text('Seleccionar todos')
                        with tag('div.flex-grow-1.text-center'):
                            if self.filter_class:
                                text('Filtrar')
                        with tag('div.flex-grow-1.text-right'):
                            if self.filter_class:
                                text('Ordenar')

            with tag('tbody'):
                for obj in self.object_list.all():
                    with tag('tr'):
                        self.render_row(obj, doc)

        return getvalue(doc)


class SimpleCreateView(CreateView):
    success_message = None

    def get_success_url(self):
        if not self.success_message:
            success_message = f'{self.object} created'
        else:
            success_message = self.success_message

        messages.success(self.request, success_message)

        return super().get_success_url()


class SimpleUpdateView(UpdateView):
    form_classes = None
    success_message = None

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_form_class(self):
        post_action = self.request.POST.get('_action', None)
        get_action = self.request.GET.get('_action', None)

        action = post_action or get_action

        if self.form_classes and action:
            if action not in self.form_classes:
                raise Http404()

            return self.form_classes[action]

        return super().get_form_class()

    def get_success_url(self):
        if not self.success_message:
            success_message = f'{self.object} Updated'
        else:
            success_message = self.success_message

        messages.success(self.request, success_message)

        return super().get_success_url()
