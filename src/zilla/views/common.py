import logging
import os
from importlib import import_module
from pathlib import Path

import tailhead
from django.conf import settings
from django.contrib.admindocs.views import extract_views_from_urlpatterns
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

logger = logging.getLogger('django.request')

content_types = {
    'json': 'application/json',
    'html': 'text/html',
    'xml': 'text/xml',
    'txt': 'text/plain',
    'md': 'text/plain',
}

__all__ = ['page_view', 'debug_view', 'logtail_view']


def page_view(request: HttpRequest, template_name: str, context=None):
    if not context:
        context = {}
    path, ext = os.path.splitext(template_name)
    content_type = content_types.get(ext, 'txt')
    return render(request, template_name=template_name, content_type=content_type, context=context)


def debug_view(request, **kwargs):
    view_functions = extract_views_from_urlpatterns(
        import_module(settings.ROOT_URLCONF).urlpatterns
    )
    view_functions = map(lambda args: (f'{args[0].__module__}:{args[0].__name__}', *args[1:]), view_functions)

    kwargs['template_name'] = 'debug/debug.html'
    kwargs['context'] = {
        'settings': settings,
        'view_functions': view_functions
    }
    return page_view(request, **kwargs)


def logtail_view(request, filepath: str='admin/logtail.html', template_name: str='admin/logtail.html', lines: int=20):
    logfile = Path(filepath)

    context = {
        'name': logfile.name,
        'path': logfile.as_posix(),
        'exists': False
    }

    exists = logfile.exists()

    try:
        content = tailhead.tail(open(logfile.resolve(), 'rb'), lines=lines)
    except:
        exists = False

    if not exists:
        return render(request, template_name, context)


    if request.is_ajax():
        return HttpResponse(content, content_type='plain/text')

    context['exists'] = True
    context['stat'] = logfile.stat()
    context['content'] = content

    return render(request, template_name, context)

