from typing import Type

from django.core.paginator import Paginator, InvalidPage
from django.db.models import QuerySet
from django.http import HttpRequest, Http404
from django.shortcuts import render
from django.utils.safestring import SafeText
from django_filters import FilterSet

from zilla.htmldoc import SimpleDoc, getvalue


class QuerysetTable:
    filter_class: Type[FilterSet] = None
    paginate_by: int = 25

    def __init__(self, request: HttpRequest, queryset: QuerySet):
        self.request = request
        self.queryset = queryset

    def __str__(self):
        return SafeText(self.as_table())


    def get_queryset(self):
        return self.queryset

    def get_paginated_queryset(self):
        filterset, queryset = self.get_filtered_queryset()
        paginator = Paginator(object_list=queryset, per_page=self.paginate_by)

        if 'page' in self.request.GET:
            page_number = self.request.GET['page']
        else:
            page_number = 1

        try:
            page_number = int(page_number)
        except ValueError:
            page_number = 1

        try:
            page = paginator.page(page_number)
        except InvalidPage:
            raise Http404('Invalid page')

        return filterset, paginator, page, page.object_list, page.has_other_pages()

    def get_filtered_queryset(self):
        if self.filter_class:
            filter_ = self.filter_class(self.request.GET, queryset=self.get_queryset())
            return filter_, filter_.qs
        return None, self.get_queryset()

    def get_object_label(self, obj):
        return str(obj)

    def get_object_options(self, obj, doc):
        return None

    def as_table(self):
        doc, tag, text = SimpleDoc().tagtext()
        filterset, paginator, page, queryset, is_paginated = self.get_paginated_queryset()

        with tag('table.object-list.table'):
            with tag('thead'):
                with tag(f'th', colspan='2'):
                    with tag('div.object-list-item.d-flex.justify-content-between'):
                        with tag('div'):
                            with tag('div.d-inline-block', style='width: 1rem;'):
                                doc.input(id='select-all', type='checkbox', name='object[]', value='all')
                            with tag('label', **{'for': 'select-all'}):
                                text('Seleccionar todos')
                        with tag('div.flex-grow-1.text-center'):
                            if self.filter_class:
                                text('Filtrar')
                        with tag('div.flex-grow-1.text-right'):
                            if self.filter_class:
                                text('Ordenar')

            with tag('tbody'):
                for obj in queryset.all():
                    with tag('tr'):
                        with tag('td.text-left'):
                            with tag('div.object-list-item'):
                                with tag('div.d-inline-block', style='width: 1rem'):
                                    doc.input(type='checkbox',
                                              id=f'object-pk-{obj.pk}',
                                              value=obj.pk,
                                              name='object[]')

                                with tag('label', **{'for': f'object-pk-{obj.pk}'}):
                                    doc.asis(self.get_object_label(obj))

                        with tag('td.text-right'):
                            self.get_object_options(obj, doc)

        return getvalue(doc)
