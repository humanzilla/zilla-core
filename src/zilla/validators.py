import re

from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _

hostname_rule = re.compile(r"^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*"
                           r"[a-zA-Z0-9])\.)*"
                           r"([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*"
                           r"[A-Za-z0-9])$", re.IGNORECASE)

hostname_validator = RegexValidator(
    hostname_rule,
    message=_('Enter a valid hostname.'),
    code='invalid',
)


def validate_hostname(value):
    """
    Raise a ValidationError exception if the value is not a valid hostname

    >>> validate_hostname("domain.com")

    >>> validate_hostname("domain-.com")
    Traceback (most recent call last):
        raise ValidationError(self.message, code=self.code)
    django.core.exceptions.ValidationError: ['Enter a valid hostname.']

    >>> validate_hostname("domain.")
    Traceback (most recent call last):
        raise ValidationError(self.message, code=self.code)
    django.core.exceptions.ValidationError: ['Enter a valid hostname.']
    """
    return hostname_validator(value)
