from bisect import insort_right
from collections import MutableMapping, defaultdict, namedtuple

from zilla.utils.apps import get_app_submodules


class BaseItem(namedtuple('Action', 'name, func, order')):
    def __gt__(self, other):
        return self.order > other.order

    def __lt__(self, other):
        return self.order < other.order

    def __eq__(self, other):
        return tuple(self) == tuple(other)


class BaseRegistry(MutableMapping):
    item_class: BaseItem
    _searched_for_items = False

    def __init__(self):
        self._hooks = defaultdict(list)

    def __getitem__(self, name):
        return self._hooks[name]

    def __setitem__(self, name, value):
        self._hooks[name].append(value)

    def __delitem__(self, name):
        del self._hooks[name]

    def __len__(self):
        return len(self._hooks)

    def __iter__(self):
        return iter(self._hooks)

    def register(self, name, order=0):
        def decorator(func):
            action = self.item_class(name, func, order)
            insort_right(self[name], action)
            return func

        return decorator

    def dispatch(self, name, *args, **kwargs):
        for action in self.get_by_name(name):
            return action.func(*args, **kwargs)

    def get_by_name(self, name, *args, **kwargs):
        for action in self[name]:
            yield action

    def search_for_items(self):
        if not self._searched_for_items:
            list(get_app_submodules('hooks'))
            self._searched_for_items = True
