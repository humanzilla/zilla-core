import logging
import re
import secrets
from collections import OrderedDict

from django.conf import settings
from django.http import HttpRequest, HttpResponse
from django.urls import reverse
from django.utils.http import is_safe_url

from zilla.contrib.hooks.registry import hooks
from zilla.utils.requests import redirect

match_html_request = re.compile(r'text/html').search

logger = logging.getLogger('django')


def nonce_middleware(get_response):
    def middleware(request: HttpRequest):
        request._nonce = secrets.token_hex(16)
        return get_response(request)

    return middleware


def shared_data_middleware(get_response):
    def middleware(request: HttpRequest):
        shared_data = OrderedDict()

        hooks.dispatch('register_shared_data', request, shared_data)
        request.shared_data = shared_data
        return get_response(request)

    return middleware


def csp_rules_middleware(get_response):
    policy = []

    for name in settings.CSP_RULES:
        rules = list(settings.CSP_RULES[name])

        if name == 'script-src':
            rules.insert(1, "'nonce-{nonce}'")
            if settings.DEBUG:
                rules.insert(1, "'unsafe-eval'")
        elif name == 'style-src':
            rules = settings.CSP_RULES[name]
            rules.insert(1, "'nonce-{nonce}'")

        policy.append(f"{name} {' '.join(rules)}")

    if settings.CSP_REPORT_URI:
        policy.append(f'report-uri {settings.CSP_REPORT_URI}')

    make_csp_string = '; '.join(policy).format

    def middleware(request: HttpRequest):
        # Set a random nonce
        response = get_response(request)  # type: HttpResponse

        if 'Content-Type' in response and 'text/html' in response['Content-Type']:
            if not settings.DEBUG:
                csp_header = "content-security-policy"
                response[csp_header] = make_csp_string(nonce=request.nonce)

        return response

    return middleware


def response_middleware(get_response):
    def middleware(request: HttpRequest):
        response = get_response(request)  # type: HttpResponse
        request_accept = request.META.get('HTTP_ACCEPT', None)

        if response.status_code == 403 and match_html_request(request_accept):
            login_url = reverse('login')

            next_url = request.GET.get('next', settings.LOGIN_REDIRECT_URL)

            safe_redirect_to = is_safe_url(
                url=next_url,
                allowed_hosts=[request.get_host()],
                require_https=request.is_secure())

            return redirect(f'{login_url}?next={safe_redirect_to}')

        return response

    return middleware
