import shlex
from functools import reduce
from typing import List
import unicodedata

from django.contrib.postgres.search import SearchQuery
from django.http import Http404
from django.shortcuts import get_object_or_404

from zilla.utils.text import remove_accents


def get_object_or_none(*args, **kwargs):
    try:
        return get_object_or_404(*args, **kwargs)
    except Http404:
        return None


COMMON_WORDS = {
    'its', 'is', 'a', 'es', 'this',
    'the', 'el', 'la', 'en', 'son', 'que'
}


def prepare_search_terms(terms: str) -> list:
    """
    Split like shell arguments, that support quotes

    >>> prepare_search_terms("Hello, this is éúóíá")
    ['hello', 'euoia']
    >>> shlex.split("a b c")
    ['a', 'b', 'c']
    >>> shlex.split('a b c "a word"')
    ['a', 'b', 'c', 'a word']
    """

    # terms = remove_accents(terms.strip())
    terms = shlex.split(terms)  # Split
    terms = [term for term in terms if term not in COMMON_WORDS]  # Filter nouns
    terms = map(lambda a: a.strip(','), terms)
    terms = map(lambda a: a.lower(), terms)
    terms = map(remove_accents, terms)
    return list(terms)


def reduce_search_query(terms: List[str], **kwargs):
    if len(terms) > 1:
        return reduce(lambda a, b: a | b, [SearchQuery(term, **kwargs) for term in terms])

    return SearchQuery(terms[0], **kwargs)
