import string
import uuid
from functools import partial

from django.conf import settings
from django.contrib.auth.hashers import mask_hash
from django.contrib.postgres.fields import JSONField, ArrayField
from django.contrib.postgres.forms import JSONField as JSONFormField
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.db.models import CharField, UUIDField, SlugField
from django.utils.crypto import get_random_string
from django.utils.functional import curry
from jsoneditor.forms import JSONEditor
from psycopg2.extras import Json
from pytz import common_timezones

from zilla.forms.widgets import FlagsCheckboxSelectMultiple
from zilla.utils.json import dumps
from zilla.utils.luhncode import LuhnCodeGenerator

__all__ = ('PermalinkField', 'PropertiesField',
           'NullCharField', 'NameField', 'StringField',
           'TimezoneChoiceField',
           'RandomCharField', 'LuhnCodeRandomField',
           'FlagsField')

TIMEZONE_CHOICES = [(timezone, timezone) for timezone in common_timezones]

NameField = partial(CharField, max_length=140, blank=False)
StringField = partial(CharField, max_length=140, blank=True)


class JSONEditorField(JSONFormField):
    widget = JSONEditor


class PermalinkField(UUIDField):
    def __init__(self, *args, **kwargs):
        kwargs['editable'] = False

        if 'primary' not in kwargs:
            kwargs['db_index'] = True

        if 'unique' not in kwargs:
            kwargs['unique'] = True

        kwargs['default'] = uuid.uuid4

        super().__init__(*args, **kwargs)


class PropertiesField(JSONField):
    def __init__(self, form_class=None, many=False, *args, **kwargs):
        if 'null' not in kwargs:
            kwargs['default'] = list if many else dict

        kwargs['blank'] = True
        kwargs['db_index'] = True
        self.many = many
        self.form_class = form_class
        super().__init__(*args, **kwargs)

    def get_prep_value(self, value):
        if value is not None:
            return Json(value, dumps=dumps)
        return value

    def to_python(self, value):
        value = super().to_python(value)
        if self.many and not isinstance(value, (list, tuple, set)):
            raise ValidationError(
                'Properties field set to hold many db, value needs to be a list, tuple or set.')
        return value

    def formfield(self, **kwargs):
        if 'form_class' not in kwargs:
            defaults = {'form_class': JSONEditorField}
            defaults.update(kwargs)
            return super().formfield(**defaults)

        return super().formfield(**kwargs)


class Creator(object):
    """
    A placeholder class that provides a way to set the attribute on the model.
    """

    def __init__(self, field):
        self.field = field

    def __get__(self, obj, type=None):
        if obj is None:
            return self
        return obj.__dict__[self.field.name]

    def __set__(self, obj, value):
        obj.__dict__[self.field.name] = self.field.to_python(value)


class NullCharField(CharField):
    """
    CharField that stores '' as None and returns None as ''
    Useful when using unique=True and forms. Implies null==blank==True.
    When a ModelForm with a CharField with null=True gets saved, the field will
    be set to '': https://code.djangoproject.com/ticket/9590
    This breaks usage with unique=True, as '' is considered equal to another
    field set to ''.
    """
    description = "CharField that stores '' as None and returns None as ''"

    def __init__(self, *args, **kwargs):
        if not kwargs.get('null', True) or not kwargs.get('blank', True):
            raise ImproperlyConfigured("NullCharField implies null==blank==True")
        kwargs['null'] = kwargs['blank'] = True
        kwargs.setdefault('max_length', 250)
        super(NullCharField, self).__init__(*args, **kwargs)

    def contribute_to_class(self, cls, name, **kwargs):
        super().contribute_to_class(cls, name, **kwargs)
        setattr(cls, self.name, Creator(self))

    def from_db_value(self, value, expression, connection, context):
        return self.to_python(value)

    def to_python(self, value):
        val = super().to_python(value)
        return val if val is not None else ''

    def get_prep_value(self, value):
        prepped = super().get_prep_value(value)
        return prepped if prepped != "" else None

    def deconstruct(self):
        """
        deconstruct() is needed by Django's migration framework
        """
        name, path, args, kwargs = super(NullCharField, self).deconstruct()
        del kwargs['null']
        del kwargs['blank']

        return name, path, args, kwargs


class TimezoneChoiceField(CharField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('max_length', 200)
        kwargs['choices'] = TIMEZONE_CHOICES
        super().__init__(*args, **kwargs)


default_chars = string.ascii_uppercase + string.ascii_lowercase + string.digits


class RandomCharField(CharField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('blank', True)
        kwargs.setdefault('max_length', 100)

        self.allowed_chars = kwargs.pop('allowed_chars', default_chars)
        self.length = kwargs.pop('length', None)

        if self.length is None:
            raise ValueError('Missing length of the random strinbg.')

        super().__init__(*args, **kwargs)

        if self.length > self.max_length:
            raise ValueError('Random string can not be more than the field max_length.')

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        kwargs['length'] = self.length
        return name, path, args, kwargs

    def get_random_option(self):
        return get_random_string(
            allowed_chars=self.allowed_chars,
            length=self.length)

    def pre_save(self, model_instance, add):
        if not add and getattr(model_instance, self.attname) != '':
            return getattr(model_instance, self.attname)

        while True:
            value = self.get_random_option()

            if model_instance._default_manager.filter(**{self.name: value}).exists():
                continue
            else:
                return value


class LuhnCodeRandomField(RandomCharField):
    def get_random_option(self):
        generator = LuhnCodeGenerator()
        return generator.encode(settings.SECRET_KEY, parts=self.length)

    def get_FIELD_mask(self, field):
        value = getattr(self, field.attname)
        return mask_hash(value)

    def contribute_to_class(self, cls, name, private_only=False):
        super(LuhnCodeRandomField, self).contribute_to_class(cls, name, private_only=private_only)
        setattr(cls, 'get_%s_mask', curry(self.get_FIELD_mask, field=self))


class FlagsField(ArrayField):
    def __init__(self, flags, size=None, **kwargs):
        base_field = SlugField(max_length=10, blank=True)
        self.flags = flags
        super(FlagsField, self).__init__(base_field=base_field, size=size, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'form_class': FlagsField,
                    'widget': FlagsCheckboxSelectMultiple(
                        choices=self.flags,
                        queryset=self.model._default_manager.filter())}

        defaults.update(kwargs)
        return super().formfield(**defaults)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        del kwargs['size']
        del kwargs['base_field']
        kwargs['flags'] = self.flags
        return name, path, args, kwargs
