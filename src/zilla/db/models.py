from collections import defaultdict
from functools import partial, reduce

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.base import ModelBase
from django.utils.functional import cached_property
from django.utils.timezone import now

# noinspection PyUnresolvedReferences
from .fields import NullCharField, PermalinkField, PropertiesField

from . import (
    append_linked_model,
    append_mutable_model,
    get_linked_models,
    get_mutable_models, )


class BaseMeta(models.base.ModelBase):
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, *args, **kwargs)


class BaseModel(models.Model, metaclass=BaseMeta):
    id = models.BigAutoField(primary_key=True)

    created = models.DateTimeField(default=now, editable=False)
    modified = models.DateTimeField(auto_now=True)

    DISPLAY_FIELDS = ['pk']

    class Meta:
        abstract = True

    def update(self, **kwargs):
        """Shortcut to update the instance using an UPDATE query"""
        queryset = type(self)._default_manager.filter(pk=self.pk)
        return queryset.update(**kwargs)

    def __str__(self):
        if hasattr(self, 'name'):
            return self.name
        return self.__repr__()

    def __repr__(self):
        cls = type(self).__name__

        pairs = ('%s=%s' % (a, repr(getattr(self, a, None)))
                 for a in self.DISPLAY_FIELDS)

        return '<{0} at 0x{1:#x} {2!s}: {3!s}>'.format(cls, id(self), self.pk, ', '.join(pairs))

    def copy(self, **kwargs):
        for field in self._meta.get_fields(include_hidden=False):
            if field.name not in kwargs:
                kwargs[field.name] = getattr(self, field.name)
        return self._meta.model(**kwargs)


class AuditedModel(BaseModel):
    deleted = models.DateTimeField(
        editable=False, null=True, blank=True)

    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='+',
        on_delete=models.CASCADE,
        editable=False, null=True, blank=True)

    modified_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='+',
        on_delete=models.CASCADE,
        editable=False, null=True, blank=True)

    deleted_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='+',
        on_delete=models.CASCADE,
        editable=False, null=True, blank=True)

    class Meta:
        abstract = True

    def save(self, **kwargs):
        self.full_clean()
        super().save(**kwargs)


LINKED_MODELS = defaultdict(list)


def limit_choices_to_linked_models(related_field):
    """Returns the registered models that can be liked/voted/etc"""
    if related_field.model._meta.abstract:
        return {}

    get_content_type = ContentType.objects.get_for_model
    models = get_linked_models()[related_field.model]
    handle_types = [get_content_type(m) for m in models]

    # Sames as Q() | Q() ... | Q()
    return reduce(lambda a, b: a | b, map(lambda ct: Q(id=ct.id), handle_types))


class LinkedForeignKey(models.ForeignKey):
    def __init__(self, *args, **kwargs):
        kwargs['limit_choices_to'] = partial(limit_choices_to_linked_models, self)
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        if 'limit_choices_to' in kwargs:
            del kwargs['limit_choices_to']
        return name, path, args, kwargs


class LinkedModel(models.Model):
    """Linked model can related to any other, like Tags, Comments, etc."""
    content_type = LinkedForeignKey('contenttypes.ContentType', on_delete=models.CASCADE)
    object_id = models.CharField(max_length=255, db_index=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        abstract = True

    @classmethod
    def register(cls, model_class):
        """Register model as able to link."""
        append_linked_model(cls, model_class)
        return model_class


class MutableMeta(ModelBase):
    def __new__(cls, name, bases, attrs):
        new_class = super().__new__(cls, name, bases, attrs)

        if new_class._meta.abstract:
            return new_class

        parents = [parent_model for parent_model in new_class._meta.parents if issubclass(parent_model, MutableModel)]

        if len(parents) > 1:
            raise RuntimeError('Mutable model has two or more mutable model parents, that is not allowed.')

        if parents:
            parent_model = parents[0]
            append_mutable_model(parent_model, new_class)
        else:
            new_class.add_to_class('get_specific_models', partial(get_mutable_models, new_class))

        return new_class


class MutableBase(models.Model, metaclass=MutableMeta):
    class Meta:
        abstract = True


class MutableModel(MutableBase):
    content_type = models.ForeignKey(
        ContentType,
        verbose_name='specific type',
        related_name='%(app_label)s_%(class)s_set+',
        null=True,
        editable=False,
        on_delete=models.PROTECT)

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.id:
            if not self.content_type_id:
                self.content_type = ContentType.objects.get_for_model(self)

    @cached_property
    def specific(self):
        content_type = ContentType.objects.get_for_id(self.content_type_id)
        model_class = content_type.model_class()
        if model_class is None:
            return self
        elif isinstance(self, model_class):
            return self
        else:
            return content_type.get_object_for_this_type(id=self.id)

    @cached_property
    def specific_class(self):
        content_type = ContentType.objects.get_for_id(self.content_type_id)
        return content_type.model_class()


class DBDefault(models.Expression):
    def __repr__(self):
        return "DEFAULT"

    def as_sql(self, compiler, connection):
        return 'DEFAULT', []


# noinspection PyUnresolvedReferences
from django.contrib.postgres.fields import *  # NOQA isort:skip
from django.db.models import *  # NOQA isort:skip
