from collections import defaultdict

LINKED_MODELS = defaultdict(list)


def get_linked_models():
    return LINKED_MODELS


def append_linked_model(linked_model, related_model):
    if related_model not in LINKED_MODELS[linked_model]:
        LINKED_MODELS[linked_model].append(related_model)


MUTABLE_MODELS = defaultdict(set)


def get_mutable_models(base_model):
    return MUTABLE_MODELS[base_model]


def append_mutable_model(parent_model, model_class):
    if model_class not in MUTABLE_MODELS[parent_model]:
        MUTABLE_MODELS[parent_model].add(model_class)
