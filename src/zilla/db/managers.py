from django.db import models


class CoreManager(models.Manager):
    _defer: tuple = None
    _select_related: tuple = None
    _prefetch_related: tuple = None

    def __init__(self, defer: tuple = None, select_related: tuple = None, prefetch_related: tuple = None):
        if defer:
            self._defer = defer

        if select_related:
            self._select_related = select_related

        if prefetch_related:
            self._prefetch_related = prefetch_related
        super().__init__()

    def get_queryset(self):
        queryset = super().get_queryset()

        if self._select_related:
            queryset = queryset.select_related(*self._select_related or None)
        if self._prefetch_related:
            queryset = queryset.prefetch_related(*self._prefetch_related or None)
        if self._defer:
            queryset = queryset.defer(*self._defer)

        return queryset
