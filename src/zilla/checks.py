from django.core.checks import Error


def check_assets_directories(app_configs, **kwargs):
    from django.conf import settings
    errors = []

    if not hasattr(settings, 'STATIC_ROOT'):
        errors.append(Error('Missing STATIC_ROOT setting'))
    if not hasattr(settings, 'MEDIA_ROOT'):
        errors.append(Error('Missing STATIC_ROOT setting'))

    settings.STATIC_ROOT.mkdir(exist_ok=True, parents=True)
    settings.MEDIA_ROOT.mkdir(exist_ok=True, parents=True)


def check_settings(app_configs, **kwargs):
    from django.conf import settings
    errors = []

    if not hasattr(settings, 'DEPLOY_VERSION'):
        errors.append(Error('Missing DEPLOY_VERSION setting', hint="""
# Add this line to your settings.py file
DEPLOY_VERSION = os.environ.get('DEPLOY_VERSION', None)
"""))

    return errors
