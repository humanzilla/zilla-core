import re
import shutil
import tempfile
from typing import Generator

from django.contrib.staticfiles.storage import ManifestStaticFilesStorage

from .utils.functional import consumer


def dispatch(value: object, *targets):
    for target in targets:
        target.send(value)


@consumer
def pager(*targets: Generator):
    while True:
        value = yield
        dispatch(value, *targets)


@consumer
def grep(pattern: str, *targets: Generator):
    pattern = re.compile(pattern)

    while True:
        value = yield
        if pattern.match(value):
            dispatch(value, *targets)


@consumer
def puts(*targets: Generator):
    while True:
        value = yield
        print('... {}'.format(value))
        dispatch(value, *targets)


class StaticStorage(ManifestStaticFilesStorage):
    def post_process(self, found_files, **options):
        paths = super().post_process(found_files, **options)

        @consumer
        def noop(*targets):
            while True:
                filename = yield
                out = tempfile.NamedTemporaryFile(delete=False)
                minified = self.open(filename).read().decode()
                out.write(minified.encode())
                shutil.move(out.name, self.path(filename))
                out.close()
                dispatch(filename, *targets)

        # TODO: reimplement Pager as a setting
        pipeline = pager(
            grep(r'^js/.+\.js$', noop()),
            grep(r'^js/.+\.css', noop()),
        )

        for name, hashed_name, processed in paths:
            pipeline.send(hashed_name)
            yield name, hashed_name, processed

        pipeline.close()
