from importlib import import_module
from typing import List, Mapping, Optional, Text

from django.conf import settings
from django.contrib.auth import SESSION_KEY, get_user_model, login, update_session_auth_hash
from django.contrib.sessions.models import Session
from django.http import HttpRequest

session_engine = import_module(settings.SESSION_ENGINE)


def get_session_dict_user(session_dict: (Mapping[Text, int])) -> Optional[int]:
    try:
        return get_user_model()._meta.pk.to_python(session_dict[SESSION_KEY])
    except KeyError:
        return None


def get_session_user(session: Session) -> int:
    return get_session_dict_user(session.get_decoded())


def user_sessions(account_id: int) -> List[Session]:
    return [s for s in Session.objects.all()
            if get_session_user(s) == account_id]


def delete_session(session: Session) -> None:
    session_engine.SessionStore(session.session_key).delete()


def delete_user_sessions(account_id: int) -> None:
    for session in Session.objects.all():
        if get_session_user(session) == account_id:
            delete_session(session)


def delete_all_user_sessions() -> None:
    for session in Session.objects.all():
        delete_session(session)


def refresh_user_session(request: HttpRequest, user):
    update_session_auth_hash(request, user)
    login(request, user)
