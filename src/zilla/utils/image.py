from colorthief import MMCQ
from PIL import ExifTags, Image


def normalize_orientation(image: Image):
    try:
        exif = image._getexif()
    except:
        exif = None

    if exif:
        orientation = exif.get(0x0112)  # ExifTags[0x0112] == Orientation

        if orientation == 2:
            image = image.transpose(Image.FLIP_LEFT_RIGHT)
        elif orientation == 3:
            image = image.rotate(180)
        elif orientation == 4:
            image = image.transpose(Image.FLIP_TOP_BOTTOM)
        elif orientation == 5:
            image = image.rotate(-90, expand=1).transpose(Image.FLIP_LEFT_RIGHT)
        elif orientation == 6:
            image = image.rotate(-90, expand=1)
        elif orientation == 7:
            image = image.rotate(90, expand=1).transpose(Image.FLIP_LEFT_RIGHT)
        elif orientation == 8:
            image = image.rotate(90, expand=1)

    return image


def square_crop(image: Image, width: int, height: int):
    image_width, image_height = image.size

    if image_width > image_height:
        delta = image_width - image_height
        left = int(delta / 2)
        upper = 0
        right = image_height + left
        lower = image_height
        resize = (height, height)
    else:
        delta = image_height - image_width
        left = 0
        upper = int(delta / 2)
        right = image_width
        lower = image_width + upper
        resize = (width, width)

    image = image.crop((left, upper, right, lower))
    image = image.resize(resize, Image.CUBIC)
    return image


def image_fit_width(image: Image, width):
    horz_scale = width / image.width
    width, height = (width, int(image.height * horz_scale))
    image = image.resize((width, height), Image.CUBIC)
    return image


EXIF_TAGS = {
    "ImageWidth",
    "ImageLength",
    "Compression",
    "Make",
    "Model",
    "Orientation",
    "XResolution",
    "YResolution",
    "ResolutionUnit",
    "Software",
    "DateTime",
    "TimeZoneOffset",
    "GPSInfo",
}


def get_exif(image: Image):
    try:
        exif = image._getexif()
    except:
        return {}

    if exif is None:
        return {}

    data = {ExifTags.TAGS[tag]: val
            for tag, val in exif.items()
            if tag in ExifTags and val in EXIF_TAGS}

    return data


def get_image_palette(image: Image, color_count=5):
    image = image.convert('RGBA')
    width, height = image.size
    pixels = image.getdata()
    pixel_count = width * height
    valid_pixels = []

    # 1. is the highest quality. The bigger the number,
    # the faster the palette will be generated
    quality = 10

    for i in range(0, pixel_count, quality):
        r, g, b, a = pixels[i]

        # If pixel is mostly opaque and not white
        if a >= 125:
            if not (r > 250 and g > 250 and b > 250):
                valid_pixels.append((r, g, b))

    # Send array to quantize function which clusters values
    # using median cut algorithm
    cmap = MMCQ.quantize(valid_pixels, color_count)

    return [u'#{:02x}{:02x}{:02x}'.format(*values) for values in cmap.palette]
