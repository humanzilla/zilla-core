import os
from contextlib import contextmanager

from django.core.cache import caches


@contextmanager
def cd(path):
    """Changes current dir while on the context"""
    old_dir = os.getcwd()

    yield os.chdir(path)

    os.chdir(old_dir)


def read_once(path):
    """Cache reading a file"""
    cache = caches['local']
    read = cache.get(path, None)

    if read:
        return read

    with open(path, 'tr') as fobj:
        read = fobj.read()

    cache.set(path, read, 60 * 60)
    return read
