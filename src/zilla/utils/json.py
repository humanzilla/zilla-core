import base64
import datetime
import decimal
import json
import uuid

from bson import ObjectId
from django.conf import settings
from django.core.files import File
from django.utils.encoding import force_text
from django.utils.functional import Promise
from rest_framework import serializers
from rest_framework.utils import encoders

from zilla.api.metadata import Metadata

__all__ = ['dumps', 'loads']

metadata = Metadata()



class JSONEncoder(encoders.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, bytes):
            # Maybe a blob
            return base64.encodebytes(obj).decode()
        elif isinstance(obj, ObjectId):
            return str(obj)
        elif isinstance(obj, uuid.UUID):
            return obj.hex
        elif isinstance(obj, Promise):
            return force_text(obj)
        elif isinstance(obj, datetime.datetime):
            representation = obj.isoformat()
            if representation.endswith('+00:00'):
                representation = representation[:-6] + 'Z'
            return representation
        elif isinstance(obj, datetime.date):
            return obj.isoformat()
        elif isinstance(obj, datetime.time):
            representation = obj.isoformat()
            return representation
        elif isinstance(obj, datetime.timedelta):
            return str(obj.total_seconds())
        elif isinstance(obj, decimal.Decimal):
            # Serializers will coerce decimals to strings by default.
            return float(obj)
        elif isinstance(obj, serializers.Serializer):
            return metadata.get_serializer_info(obj)
        elif hasattr(obj, '_asdict'):
            return obj._asdict()
        elif isinstance(obj, File):
            return str(obj.name)
        return super().default(obj)


def dumps(obj):
    ret = json.dumps(obj, indent='  ' if settings.DEBUG else None, cls=JSONEncoder, ensure_ascii=False)
    ret = ret.replace('\u2028', '\\u2028').replace('\u2029', '\\u2029')
    return ret


def loads(data):
    return json.loads(data)


json_decode = loads
json_encode = dumps
