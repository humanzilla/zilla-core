import cProfile
import pstats
import tempfile
from functools import wraps
from time import time
from uuid import UUID, uuid5

FINGERPRINT_PARTS = [
    'HTTP_ACCEPT_ENCODING',
    'HTTP_ACCEPT_LANGUAGE',
    'HTTP_USER_AGENT',
    'HTTP_X_FORWARDED_FOR',
    'REMOTE_ADDR']

UUID_NAMESPACE = UUID('611d2fd8-bc08-11e5-845d-60a44cb01ca6')


def get_client_fingerprint(request):
    parts = [request.META.get(key, '') for key in FINGERPRINT_PARTS]
    return uuid5(UUID_NAMESPACE, '_'.join(parts))


def timeit(func):
    @wraps(func)
    def wrapper(*args, **kwds):
        start = time()
        result = func(*args, **kwds)
        elapsed = time() - start
        print("%s took %d time to finish" % (func.__name__, elapsed))
        return result

    return wrapper


def profileit(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        prof = cProfile.Profile()

        with tempfile.NamedTemporaryFile() as temp:
            retval = prof.runcall(func, *args, **kwargs)
            prof.dump_stats(temp.name)
            stats = pstats.Stats(temp.name)

        stats.strip_dirs()
        stats.sort_stats('cumulative')
        stats.print_stats(20)

        return retval

    return wrapper
