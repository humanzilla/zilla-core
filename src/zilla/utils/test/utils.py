from unittest import mock

from django.contrib.auth.models import AnonymousUser
from django.contrib.messages.storage.fallback import FallbackStorage
from django.contrib.sessions.backends.db import SessionStore
from django.db.models import Model
from django.test import RequestFactory as BaseRequestFactory


class RequestFactory(BaseRequestFactory):
    def request(self, user=None, **request):
        request = super(RequestFactory, self).request(**request)
        request.user = user or AnonymousUser()
        request.session = SessionStore()
        request._messages = FallbackStorage(request)

        return request


class _ModelMock(mock.Mock):
    def __init__(self, *args, **kwargs):
        super(_ModelMock, self).__init__(*args, **kwargs)

        # Django ORM needed state for write status
        self._state = mock.Mock()
        self._state.db = None

    def _get_child_mock(self, **kwargs):
        name = kwargs.get('name', '')
        if name == 'pk':
            return self.id
        return super(_ModelMock, self)._get_child_mock(**kwargs)


def model_mock_factory(model: Model):
    """Returns a usable mock instance from a model Class"""
    return _ModelMock(spec=model())
