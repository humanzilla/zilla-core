from unittest import mock


def compose(*functions):
    """
    Compose functions
    This is useful for combining decorators.
    """

    def _composed(*args):
        for fn in functions:
            try:
                args = fn(*args)
            except TypeError:
                # args must be scalar so we don't try to expand it
                args = fn(args)
        return args

    return _composed


no_database = mock.patch('django.db.backends.util.CursorWrapper',
                         mock.Mock(side_effect=RuntimeError("Using the database is not permitted!")))

no_filesystem = mock.patch('__builtin__.open',
                           mock.Mock(side_effect=RuntimeError("Using the filesystem is not permitted!")))

no_sockets = mock.patch('socket.getaddrinfo',
                        mock.Mock(side_effect=RuntimeError("Using sockets is not permitted!")))

no_externals = no_diggity = compose(no_database, no_filesystem, no_sockets)
