import contextlib
from unittest.mock import Mock


@contextlib.contextmanager
def mock_signal_receiver(signal, wraps=None, **kwargs):
    """
    Temporarily attaches a receiver to the provided ``signal`` within the scope
    of the context manager.

    The mocked receiver is returned as the ``as`` target of the ``with``
    statement.

    To have the mocked receiver wrap a callable, pass the callable as the
    ``wraps`` keyword argument. All other keyword arguments provided are passed
    through to the signal's ``connect`` method.

    Example:


    >>> with mock_signal_receiver(post_save, sender=Model) as receiver:  # doctest: +SKIP
    ...     Model.objects.create()
    ...     assert receiver.call_count == 1

    """
    __test__ = False

    if wraps is None:
        def wraps(*args, **kwrags):
            return None

    receiver = Mock(wraps=wraps)
    signal.connect(receiver, **kwargs)
    yield receiver
    signal.disconnect(receiver)
