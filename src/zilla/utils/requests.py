import re
from collections import OrderedDict
from urllib.parse import urlunparse

from django.http import HttpResponseRedirect, HttpRequest
from django.utils.http import is_safe_url

absolute_http_url_re = re.compile(r'^https?://', re.IGNORECASE)

__all__ = ['get_headers', 'get_ip', 'build_url', 'get_local_host', 'redirect']


def get_headers(request: HttpRequest):
    wsgi_env = list(sorted(request.META.items()))
    return OrderedDict((k.replace('_', ' '), v)
                       for (k, v) in wsgi_env if k.startswith('HTTP_') or k.startswith('REMOTE_'))


def get_ip(request: HttpRequest):
    ip = request.META.get('HTTP_X_FORWARDED_FOR', None)
    ip = ip.split(', ')[0] if ip else request.META.get('REMOTE_ADDR', '')
    return ip


def build_url(scheme='', host='', path='', params='', query='', fragment=''):
    return urlunparse((scheme, host, path, params, query, fragment))


def get_local_host(request: HttpRequest):
    scheme = 'http' + ('s' if request.is_secure() else '')
    return build_url(scheme=scheme, host=request.get_host())


def redirect(url: str):
    if not is_safe_url(url, allowed_hosts='*'):
        url = '/'
    return HttpResponseRedirect(url)
