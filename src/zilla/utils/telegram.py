from collections import namedtuple

import requests
from django.conf import settings


class Telegram:
    api_url = 'https://api.telegram.org/bot{token}/{method}'.format
    Button = namedtuple('Button', ['text', 'url'])

    def command(self, name, data):
        url = self.api_url(token=settings.TELEGRAM_ACCESS_TOKEN, method=name)
        return requests.post(url=url, json=data)

    def sendMessage(self,
                    text: str,
                    chat_id: str = None,
                    buttons: list = None,
                    notify=True):

        if not chat_id:
            chat_id = settings.TELEGRAM_CHAT_ID

        data = {'text': text,
                'chat_id': chat_id,
                'parse_mode': 'markdown',
                'disable_notification': not notify}

        if buttons:
            data['reply_markup'] = {'inline_keyboard': buttons}

        return self.command('sendMessage', data)


telegram = Telegram()
