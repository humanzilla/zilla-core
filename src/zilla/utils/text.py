import re
import string
import unicodedata
from functools import wraps

import bleach
from django.db import transaction
from django.utils.safestring import mark_safe
from django.utils.text import slugify

punctuation_translate = str.maketrans({key: None for key in string.punctuation})

camelcase_sub = re.compile(r'(((?<=[a-z])[A-Z])|([A-Z](?![A-Z]|$)))').sub


def camelcase_to_underscore(value: str) -> str:
    """¡
    Transform a camelcase string into a underscore separated string

    >>> camelcase_to_underscore('MyModel')
    'my_model'
    >>> camelcase_to_underscore('wordWordWordWord')
    'word_word_word_word'
    """
    # http://djangosnippets.org/snippets/585/
    return camelcase_sub('_\\1', value).lower().strip('_')


def make_safe_html(text):
    """Return a safe and clean html."""
    return mark_safe(bleach.clean(
        text,
        tags=['p', 'b', 'em', 'del', 'strong', 'ul', 'li', 'ol', 'br']))


def marksafe(func):
    return mark_safe(func)

def safe_html(func):
    @wraps(func)
    def inner(*args, **kwargs):
        ret = func(*args, **kwargs)
        return make_safe_html(ret)

    return inner


@transaction.atomic
def unique_slugify(text, field, queryset):
    slug = slugify(text)
    tries = 1

    while queryset.filter(**{field: slug}).exists():
        slug = '%s-%s' % (slug, tries)
        tries += 1

    return slug


def readablesize(num: int, suffix='B'):
    """
    Returns a human readable value for a byte value

    >>> readablesize(1024)
    '1.0KiB'
    >>> readablesize(1030) # Rounding
    '1.0KiB'
    >>> readablesize(1024 * 2)
    '2.0KiB'
    >>> readablesize(1024 * 10, 'b')
    '10.0Kib'
    >>> readablesize(1024 * 100, 'b')
    '100.0Kib'
    >>> readablesize(1024 * 2000, 'b')
    '2.0Mib'
    >>> readablesize(1024 * 40000, 'b')
    '39.1Mib'
    >>> readablesize(1024 * 400000, 'b')
    '390.6Mib'

    """
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def title_slugify(value: str) -> str:
    """
    Slugify a string useful for SEO and mode Human Readable

    >>> title_slugify('Hello my name is Mario César Señoranis Ayala')
    'Hello-my-name-is-Mario-César-Señoranis-Ayala'
    >>> title_slugify('')
    ''
    >>> title_slugify('   Word  Word    Word  ')
    'Word-Word-Word'
    >>> title_slugify(' ---  Word  Word    Word  --- ')
    '-Word-Word-Word-'

    """
    value = unicodedata.normalize('NFKC', value)
    value = re.sub(r'[^\w\s-]', '', value, flags=re.U).strip()
    return mark_safe(re.sub(r'[-\s]+', '-', value, flags=re.U))


def remove_accents(value: str) -> str:
    """
    Remove accents from the string

    >>> remove_accents("á")
    'a'
    >>> remove_accents("áéíóú")
    'aeiou'
    >>> remove_accents("á é í o ñ")
    'a e i o n'
    """
    value.translate(punctuation_translate)  # Cleanup from punctuation
    nfkd_form = unicodedata.normalize('NFKD', value)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    return only_ascii.decode('utf8')
