from email.mime.image import MIMEImage
from hashlib import sha1

from django.conf import settings
from django.core.files.storage import get_storage_class
from django.core.mail import EmailMultiAlternatives, get_connection
from lxml import etree
from lxml.cssselect import CSSSelector
from premailer import Premailer

from .decorators import service

__all__ = ['send_mail']


def embed_images(page_root):
    attachments = {}

    static = get_storage_class(settings.STATICFILES_STORAGE)()
    media = get_storage_class(settings.DEFAULT_FILE_STORAGE)()

    def embed_image(tag):
        old_src = tag.attrib.get('src')

        attach = None

        if old_src.startswith(settings.STATIC_URL):
            old_src = old_src[len(settings.STATIC_URL):]

            if static.exists(old_src):
                attach = static.open(old_src)

        elif old_src.startswith(settings.MEDIA_URL):
            old_src = old_src[len(settings.MEDIA_URL):]

            if media.exists(old_src):
                attach = media.open(old_src)

        if attach:
            cid = sha1(old_src.encode()).hexdigest()
            new_src = 'cid:%s' % cid
            tag.attrib['src'] = new_src

            if cid not in attachments:
                msg_img = MIMEImage(attach.read())
                attach.close()

                msg_img.add_header('Content-ID', '<{}>'.format(cid))
                attachments[cid] = msg_img

    for imgtag in CSSSelector('img[src].embeded')(page_root):
        embed_image(imgtag)

    return attachments.values()


@service
def send_mail(**kwargs):
    # TODO: Log email sent
    # extra = {'message_id': message.extra_headers['Message-Id'],
    #          'size': len(message.message().as_bytes())}

    # Create root node
    content = kwargs['body'].strip()
    parser = etree.HTMLParser()
    root = etree.fromstring(content.strip(), parser)

    # Collect embeded images
    mime_attachs = [mime_attach for mime_attach in embed_images(root)]

    # Transform html content
    pre_mailer = Premailer(
        root,
        disable_validation=True,
        include_star_selectors=True)

    root = pre_mailer.transform()
    kwargs['body'] = etree.tostring(root, method='html', encoding='utf-8').decode()

    # Build email message
    message = EmailMultiAlternatives(**kwargs)
    message.content_subtype = 'html'
    message.mixed_subtype = 'related'

    for attach in mime_attachs:
        message.attach(attach)

    connection = get_connection()

    try:
        connection.open()
        connection.send_messages([message])
    finally:
        connection.close()
