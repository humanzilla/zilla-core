from contextlib import contextmanager


@contextmanager
def disconnect_signal(signal, receiver, **kwargs):
    msg = f'Unable to disconnect {signal} {kwargs}'
    assert signal.disconnect(receiver, **kwargs), msg

    yield

    signal.connect(receiver, **kwargs)
