import logging
import os
from datetime import datetime

import django
from django.core.handlers.wsgi import WSGIHandler

has_newrelic = False

try:
    import newrelic.agent

    has_newrelic = True
except ImportError:
    pass


class WSGIApplication(WSGIHandler):
    def __init__(self, *args, **kwargs):
        django.setup(set_prefix=False)
        os.environ.setdefault("DEPLOY_VERSION", "develop")
        os.environ.setdefault("DEPLOY_DATETIME", datetime.utcnow().isoformat())

        deploy_version = os.environ.get("DEPLOY_VERSION")
        deploy_datetime = os.environ.get("DEPLOY_DATETIME")

        self.served_by = 'rev={};date={}'.format(deploy_version, deploy_datetime)

        logging.getLogger('django').info('Starting wsgi application. Version: %s', deploy_version)

        super().__init__(*args, **kwargs)

    def get_response(self, request):
        response = super().get_response(request)
        response['X-Served-By'] = self.served_by

        if 'production' in os.environ.get('DJANGO_SETTINGS_MODULE'):
            if has_newrelic and hasattr(request, 'user'):
                newrelic.agent.add_custom_parameter('user_id', request.user.id)

        return response
