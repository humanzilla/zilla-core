import django.dispatch

user_created = django.dispatch.Signal(providing_args=["instance"])
user_joined = django.dispatch.Signal(providing_args=["request", "instance"])
invalid_password = django.dispatch.Signal(providing_args=["username", "password"])
