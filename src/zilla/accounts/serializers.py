from django.contrib.auth import authenticate, get_user_model, password_validation
from django.utils.http import urlsafe_base64_decode
from rest_framework.exceptions import ValidationError

from zilla.api import serializers
from zilla.utils.sessions import refresh_user_session

UserModel = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = ('id', 'first_name', 'last_name', 'is_staff')


class LogoutSerializer(serializers.Serializer):
    logout_from_all = serializers.BooleanField(default=False, label='Logout from all devices')


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(label="Correo Electronico", min_length=10)
    password = serializers.PasswordField(label="Contraseña")
    remember = serializers.BooleanField(default=False)

    user = None

    def validate(self, attrs):
        request = self.context['request']

        self.user = authenticate(
            request,
            username=attrs['email'],
            password=attrs['password'])

        if self.user is None:
            raise ValidationError('Invalid login')

        return attrs


class SignupUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = ('email', 'first_name', 'last_name')


class ActivateUserSerializer(serializers.Serializer):
    _user = None

    def _get_user(self):
        if self._user is not None:
            return self._user

        request = self.context['request']
        uidb64 = request.GET.get('o', None)

        if uidb64 is None:
            raise ValidationError("Missing token")

        try:
            # urlsafe_base64_decode() decodes to bytestring
            uid = urlsafe_base64_decode(uidb64).decode()
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            raise ValidationError("Unknown token")

        self._user = user

        return user

    def validate(self, data):
        if not self._get_user():
            raise ValidationError("Invalid confirmation request")

        return data

    def save(self):
        user = self._get_user()
        user.is_active = True
        user.save(update_fields=['is_active'])
        return user


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()


class SetPasswordSerializer(serializers.Serializer):
    new_password1 = serializers.PasswordField()
    new_password2 = serializers.PasswordField()

    def validate(self, data):
        password1 = data.get('new_password1')
        password2 = data.get('new_password2')

        if password1 and password2:
            if password1 != password2:
                raise serializers.ValidationError({
                    "new_password1": "Passwords don't match",
                    "new_password2": "Passwords don't match"
                })

        password_validation.validate_password(password2)

        return data


class PasswordConfirmChangeSerializer(SetPasswordSerializer):
    old_password = serializers.PasswordField()

    def validate(self, data):
        data = super().validate(data)
        user = self.context['request'].user

        old_password = data.get('old_password')
        new_password = data.get('new_password1')

        if not user.check_password(old_password):
            raise serializers.ValidationError({"old_password": "Wrong password"})

        try:
            password_validation.validate_password(new_password)
        except ValidationError as error:
            raise serializers.ValidationError({
                "new_password1": error.detail,
                "new_password2": error.detail
            })

        return data

    def save(self):
        request = self.context['request']

        user = request.user
        user.set_password(self.validated_data['new_password1'])
        user.save(update_fields=['password'])

        refresh_user_session(request, user)

        return user
