from django import forms


class UserSettingsForm(forms.Form):
    timezone = forms.CharField()
    language = forms.CharField()
