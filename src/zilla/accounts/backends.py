from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

from .signals import invalid_password


class UserBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()

        if '@' in username:
            username_field = 'email__iexact'
        else:
            username_field = 'username__iexact'

        try:
            user = UserModel.objects.get(**{username_field: username})
        except UserModel.DoesNotExist:
            # Reduce the timing by calculating the password again, this to avoid exploits
            UserModel().set_password(password)
        else:
            if self.check_password(user, password):
                return self.check_user_active(user)
            else:
                self.wrong_password(username, password)

        return None

    def check_user_active(self, user):
        if user.is_locked():
            # TODO: log locked user attempt to login
            return False
        return user

    def check_password(self, user, password):
        if user.check_password(password):
            return True
        return False

    def wrong_password(self, username, password):
        invalid_password.send(sender=type(self), username=username, password=password)
