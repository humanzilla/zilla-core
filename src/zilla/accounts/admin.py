from django import forms
from django.contrib import messages
from django.contrib.admin.utils import unquote
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin, sensitive_post_parameters_m
from django.contrib.auth.forms import UserChangeForm as BaseUserChangeForm
from django.core.exceptions import PermissionDenied
from django.db import models
from django.forms import widgets
from django.http import Http404, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import path, reverse
from django.utils.translation import ugettext_lazy as _

UserModel = get_user_model()


class UsernameChangeForm(forms.ModelForm):
    username = forms.CharField(
        required=False,
        help_text='Leave it blank to generate a random username.')

    accept = forms.BooleanField(
        help_text='Accept that this will have side effects'
    )

    class Meta:
        model = UserModel
        fields = ('username',)

    def clean_accept(self):
        if not self.cleaned_data['accept']:
            raise forms.ValidationError('')
        return self.cleaned_data['accept']


class UserChangeForm(BaseUserChangeForm):
    username = forms.CharField(
        required=False,
        disabled=True,
        help_text=(
            "<a href=\"../update-username/\">Cambiar nombre de usuario</a>."
        ), )

    class Meta:
        model = UserModel
        fields = '__all__'


class BaseUserAdmin(UserAdmin):
    ordering = ('-date_joined',)
    list_display = ('username', 'email', 'is_active', 'is_staff', 'date_joined', 'last_login')
    list_filter = ('is_staff', 'is_superuser', 'is_active')
    search_fields = ('email', 'first_name', 'last_name')
    date_hierarchy = 'date_joined'
    form = UserChangeForm
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password')}),
        (_('Personal info'), {
            'fields': ['first_name', 'last_name', 'photo']
        }),
        (_('Important dates'), {
            'fields': [
                'date_joined',
                'blocked_until',
                'last_login',
                'last_login_ip',
                'last_login_attempt',
                'last_login_attempt_ip'
            ]
        }),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser')
        }),
    )

    readonly_fields = [
        'date_joined',
        'last_login',
        'last_login_ip',
        'last_login_attempt',
        'last_login_attempt_ip'
    ]

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
        ('User information', {
            'fields': ('first_name', 'last_name')
        }),
    )

    formfield_overrides = {
        models.TextField: {'widget': widgets.Textarea(attrs={'rows': 3, 'cols': 80})},
    }

    def get_urls(self):
        update_username_view = self.admin_site.admin_view(self.update_username_view)
        return [path('<id>/update-username/', update_username_view,
                     name='auth_user_password_change')] + super().get_urls()

    @sensitive_post_parameters_m
    def update_username_view(self, request, id, form_url=''):
        if not self.has_change_permission(request):
            raise PermissionDenied

        user = self.get_object(request, unquote(id))

        if not user:
            raise Http404()

        form = UsernameChangeForm(data=request.POST or None)

        if request.method == 'POST' and form.is_valid():
            user.username = form.cleaned_data['username']
            user.save(update_fields=['username'])

            change_message = self.construct_change_message(request, form, None)
            self.log_change(request, user, change_message)
            messages.success(request, 'Username was updated')

            return HttpResponseRedirect(
                reverse(
                    '%s:%s_%s_change' % (
                        self.admin_site.name,
                        user._meta.app_label,
                        user._meta.model_name,
                    ),
                    args=(user.pk,),
                )
            )

        context = {
            'title': 'Update username',
            'form': form,
            'original': user,
            'opts': self.model._meta,
        }
        context.update(self.admin_site.each_context(request))

        return TemplateResponse(
            request,
            template='admin/auth/user/update_username.html',
            context=context)
