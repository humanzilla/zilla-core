from django.contrib.auth import get_user_model, logout
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from django.utils.http import urlsafe_base64_encode
from rest_framework import permissions, viewsets

from zilla.accounts.serializers import (
    ActivateUserSerializer, LoginSerializer,
    LogoutSerializer, PasswordResetSerializer,
    SetPasswordSerializer, SignupUserSerializer,
    UserSerializer, )

from zilla.api.decorators import action_handler
from zilla.api.response import Response
from zilla.utils.sessions import delete_user_sessions, refresh_user_session
from .utils import get_next_url

UserModel = get_user_model()


class AuthViewSet(viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny,)

    @action_handler(LoginSerializer)
    def login(self, request, serializer):
        refresh_user_session(request, serializer.user)

        if serializer.data['remember']:
            request.session.set_expiry(0)

        return Response({
            'expiry_date': request.session.get_expiry_date(),
            'user': dict(UserSerializer(instance=serializer.user).data)
        }, status=201, headers={'Location': '/'})

    @action_handler(LogoutSerializer)
    def logout(self, request, serializer):
        logout(request)

        if serializer.data['logout_from_all']:
            delete_user_sessions(request.user.id)

        next_page = get_next_url(request)

        if next_page:
            return redirect(next_page)

        return Response(serializer.data, status=200)

    @action_handler(SignupUserSerializer)
    def signup(self, request, serializer):
        serializer.save()
        return Response(serializer.data, status=202)

    @action_handler(ActivateUserSerializer)
    def signup_activation(self, request, serializer):
        serializer.save()
        refresh_user_session(request, serializer.user)
        return Response({'messages': ['Your user is now active']}, status=202)

    @action_handler(PasswordResetSerializer)
    def password_reset(self, request, serializer):
        try:
            user = UserModel.objects.filter(
                email__iexact=serializer.data['email'],
                is_active=True
            ).get()
        except ObjectDoesNotExist:
            # TODO: Log intent to reset the password of an unknown user
            pass
        else:
            context = {
                'email': serializer.data['email'],
                'uid': urlsafe_base64_encode(bytes(user.pk)).decode(),
                'user': user,
                'token': default_token_generator.make_token(user),
                'protocol': 'https' if request.is_secure() else 'http',
            }

            # send_mail

            return Response({'messages': ['An email with a link to recover your password has been sent']}, status=200)

    @action_handler(SetPasswordSerializer)
    def password_reset_confirm(self, request, serializer):
        serializer.save()
        refresh_user_session(request, serializer.user)
        return Response({'messages': ['Password was reset. login again with your new password']})
