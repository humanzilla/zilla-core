from django.contrib.auth import get_user_model
from rest_framework import permissions, viewsets
from rest_framework.response import Response

from zilla.accounts.serializers import PasswordConfirmChangeSerializer, UserSerializer
from zilla.api.decorators import action_handler

UserModel = get_user_model()


class AccountViewSet(viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request):
        serializer = UserSerializer(instance=request.user)
        return Response({'serializer': serializer}, status=200)

    @action_handler(PasswordConfirmChangeSerializer)
    def password_change(self, request, serializer):
        serializer.save()
        return Response({'messages': ['Password was changed']}, status=200)
