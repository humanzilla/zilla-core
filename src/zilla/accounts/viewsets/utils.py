from django.conf import settings
from django.http import HttpRequest
from django.shortcuts import resolve_url
from django.utils.http import is_safe_url


def get_next_url(request: HttpRequest):
    """Returns a valid url to redirect after a sucesfull login or register"""
    next_url = resolve_url(settings.LOGOUT_REDIRECT_URL)

    if ('next' in request.POST or 'next' in request.GET):
        next_url = request.POST.get('next', request.GET['next'])

        url_is_safe = is_safe_url(
            url=next_url,
            allowed_hosts=[request.get_host()],
            require_https=request.is_secure(),
        )

    return next_url
