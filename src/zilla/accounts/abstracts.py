from datetime import timedelta

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class AbstractUserLock(models.Model):
    blocked_at = models.DateTimeField(_('Blocked at'), null=True, blank=True)
    blocked_until = models.DateTimeField(_('Blocked until'), null=True, blank=True)

    class Meta:
        abstract = True

    def activate(self):
        if not self.is_active:
            self.is_active = True
            self.save(update_fields=('is_active',))

    def lockout(self, minutes=15):
        """Flag the user with a block for the next `minutes`."""
        self.blocked_until = timezone.now() + timedelta(minutes)
        self.save(update_fields=['blocked_until'])

    def unlock(self):
        """Remove the block flag for the user if there is any."""
        self.blocked_until = None
        self.save(update_fields=['blocked_until'])

    def is_locked(self):
        """Get if the user has block flag or not."""
        if self.blocked_until:
            return self.blocked_until < timezone.now()
        return False

    is_locked.boolean = True
