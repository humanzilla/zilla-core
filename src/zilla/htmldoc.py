import re
from functools import partial

from django.conf import settings
from django.contrib.staticfiles.finders import find
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.cache import caches
from django.utils.safestring import SafeText
from yattag import Doc, indent

if settings.DEBUG:
    getvalue = lambda doc: indent(doc.getvalue())
    openstatic = lambda path: open(find(path), 'rt', buffering=10)
else:
    getvalue = lambda doc: doc.getvalue()
    openstatic = lambda path: staticfiles_storage.open(path, 'rt')

cache = caches['local']

tagname = re.compile(
    r'^(?P<name>[a-z0-9\-_]+)(\.?(?P<classname>[a-z0-9\.\-_]+))?(\#(?P<identifier>[a-z0-9\-_]+))?',
    flags=re.IGNORECASE).match


class SimpleDoc(Doc):
    class Tag(Doc.Tag):
        def __init__(self, doc, name, attrs):  # name is the tag name (ex: 'div')
            """
            Alias div.classname#identifier
            """

            groups = tagname(name).groupdict()

            self.doc = doc
            self.name = groups['name']

            if groups['classname']:
                classname = ' '.join(groups['classname'].split('.'))
                attrs['class'] = f'{classname} {attrs["class"]}' if 'class' in attrs else classname

            if groups['identifier']:
                attrs['id'] = groups['identifier']

            self.attrs = attrs

    def __html__(self):
        return SafeText(getvalue(self))


class HtmlRenderer:
    """
    HtmlRenderer is a helper for yattag

    >>> from unittest.mock import MagicMock

    >>> request = MagicMock()
    >>> renderer = HtmlRenderer(request)
    >>> doc = renderer.doc
    >>> text = renderer.text

    >>> with doc.tag('html'):
    ...     with doc.tag('head'):
    ...         with doc.tag('title'):
    ...             text('Documen title')
    >>> print(renderer)
    <html><head><title>Documen title</title></head></html>
    >>> renderer.__html__()
    '<html><head><title>Documen title</title></head></html>'
    >>> str(renderer)
    '<html><head><title>Documen title</title></head></html>'
    >>> repr(renderer)
    "<HtmlRenderer value='<html><head><title>Documen title</title></head></html>'>"
    """

    def __init__(self, request):
        self.request = request
        doc, tag, text = SimpleDoc().tagtext()
        self.doc = doc
        self.tag = tag
        self.text = text
        self.meta = partial(doc.stag, 'meta')
        self.link = partial(doc.stag, 'link')
        self.img = partial(doc.stag, 'img')
        self.div = partial(doc.tag, 'div')
        self.button = partial(doc.tag, 'button')

        if not hasattr(request, '_scripts'):
            setattr(request, '_scripts', [])

        if not hasattr(request, '_stylesheets'):
            setattr(request, '_stylesheets', [])

        self._nonce = getattr(self.request, '_nonce')

    def __repr__(self):
        return f'<{self.__class__.__name__} value={str(self)!r}>'

    def __str__(self):
        return SafeText(getvalue(self.doc))

    def __html__(self):
        return SafeText(getvalue(self.doc))

    def stylesheet(self, *, href: str, inline: bool = False, **kwargs):
        if not inline:
            self.link(href=href, rel='preload', **{'as': 'style'})

        if inline:
            content = cache.get(href)

            if not content:
                with openstatic(href) as fobj:
                    content = fobj.read()
                    cache.set(href, content)

        def render_inline():
            nonlocal content

            with self.doc.tag('style'):
                self.text(content)

        def render_link():
            self.doc.stag('link',
                          rel='stylesheet',
                          href=href,
                          nonce=self._nonce,
                          **kwargs)

        self.request._stylesheets.append(render_inline if inline else render_link)


    def script(self, *, src, **kwargs):
        self.request._scripts.append(dict(src=src, **kwargs))

    def preload_scripts(self):
        for kwargs in self.request._scripts:
            self.link(href=kwargs['src'], rel='preload', **{'as': 'script'})

    def render_scripts(self):
        for kwargs in self.request._scripts:
            with self.doc.tag('script', nonce=self._nonce, **kwargs):
                self.doc.text('')

    def render_stylesheets(self):
        for render in self.request._stylesheets:
            render()
