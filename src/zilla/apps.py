from django.apps import AppConfig
from django.core.checks import register

from .checks import check_settings


class CoreConfig(AppConfig):
    name = 'zilla'
    verbose_name = 'Core'

    def ready(self):
        register(check_settings)
